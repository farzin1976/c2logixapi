﻿using System.Collections.Generic;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using MediatR;

namespace Fleetmind.Services.integration.Optimization.Application.OptimizedProjects.Queries
{
    //ToDo:Create DTO for return type
    public class GetSubmitedProjectQuery:IRequest<SubmitedProject>
    {
        public string InternalRefrenceId { get; set; }
    }
}
