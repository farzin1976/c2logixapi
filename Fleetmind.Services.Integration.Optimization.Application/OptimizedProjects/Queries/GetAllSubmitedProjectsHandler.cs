﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.Persistence;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Fleetmind.Services.integration.Optimization.Application.OptimizedProjects.Queries
{
    public class GetAllSubmitedProjectsHandler : IRequestHandler<GetAllSubmitedProjectQuery, List<SubmitedProject>>
    {
        private readonly OptimizerDbContext _optimizerDbContext;
        public GetAllSubmitedProjectsHandler(OptimizerDbContext optimizerDbContext)
        {
            _optimizerDbContext = optimizerDbContext;
        }
        public Task<List<SubmitedProject>> Handle(GetAllSubmitedProjectQuery request, CancellationToken cancellationToken)
        {
            return _optimizerDbContext.submitedProjects.ToListAsync(cancellationToken);
        }
    }
}
