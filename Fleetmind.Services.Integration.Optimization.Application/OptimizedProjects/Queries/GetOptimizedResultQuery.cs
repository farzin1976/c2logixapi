﻿using Fleetmind.Services.integration.Optimization.Application.DTO;
using MediatR;
using System.Collections.Generic;

namespace Fleetmind.Services.integration.Optimization.Application.OptimizedProjects.Queries
{
    //ToDo: needs to add duration excceded end time 
    public class GetOptimizedResultQuery:IRequest<List<OptimizerRespnoseDTO>>
    {
        public string InternalRefrenceId { get; set; }
    }
}
