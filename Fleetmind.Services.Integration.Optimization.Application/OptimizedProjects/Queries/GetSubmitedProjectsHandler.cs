﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.Persistence;
using MediatR;

namespace Fleetmind.Services.integration.Optimization.Application.OptimizedProjects.Queries
{

    public class GetSubmitedProjectsHandler : IRequestHandler<GetSubmitedProjectQuery, SubmitedProject>
    {
        private readonly OptimizerDbContext _optimizerDbContext;
        public GetSubmitedProjectsHandler(OptimizerDbContext optimizerDbContext)
        {
            _optimizerDbContext = optimizerDbContext;
        }
        public async Task<SubmitedProject> Handle(GetSubmitedProjectQuery request, CancellationToken cancellationToken)
        {
            return  _optimizerDbContext.submitedProjects.FirstOrDefault(c => c.InternalRefrenceId == request.InternalRefrenceId);
        }
    }
}
