﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using MediatR;
using System.Collections.Generic;

namespace Fleetmind.Services.integration.Optimization.Application.OptimizedProjects.Queries
{

    public class GetAllSubmitedProjectQuery:IRequest<List<SubmitedProject>>
    {

    }
}
