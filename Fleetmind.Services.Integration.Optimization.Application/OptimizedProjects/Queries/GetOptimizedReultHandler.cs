﻿using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Fleetmind.Services.integration.Optimization.Application.Abstractions;
using Fleetmind.Services.integration.Optimization.Application.DTO;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using Fleetmind.Services.Integration.Optimization.Persistence;
using FleetMind.C2Logix.HttpCalls;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Fleetmind.Services.integration.Optimization.Application.OptimizedProjects.Queries
{
    public class GetOptimizedReultHandler : IRequestHandler<GetOptimizedResultQuery, List<OptimizerRespnoseDTO>>
    {
        private readonly IFileSystem _fileSystem;
        private readonly OptimizerDbContext _optimizerDbContext;
        private readonly IOptimizerClient _c2Logix;
        private readonly ILogger _logger;
        private readonly IProcessReport<OptimizerResponseModel> _processReport;
        private readonly IConfig _config;
        public GetOptimizedReultHandler(IFileSystem fileSystem,
                                      OptimizerDbContext optimizerDbContext,
                                      IOptimizerClient c2Logix,
                                      ILogger<GetOptimizedReultHandler> logger,
                                      IProcessReport<OptimizerResponseModel> processReport,
                                      IConfig config)
        {
            _fileSystem = fileSystem;
            _optimizerDbContext = optimizerDbContext;
            _c2Logix = c2Logix;
            _logger = logger;
            _processReport = processReport;
            _config = config;

        }
        public  async Task<List<OptimizerRespnoseDTO>> Handle(GetOptimizedResultQuery request, CancellationToken cancellationToken)
        {
            List<OptimizerRespnoseDTO> retValue = new List<OptimizerRespnoseDTO>();
            var submitedProject= _optimizerDbContext.submitedProjects.FirstOrDefault(c => c.InternalRefrenceId == request.InternalRefrenceId);
            //ToDo:Add New Exception
            if (submitedProject == null)
                return retValue;

              
            if(submitedProject.IsCompleted)
               if(_optimizerDbContext.optimizerResponses.Where(c=>c.InternalRefrenceId==request.InternalRefrenceId).Any())
                {
                    var result = _optimizerDbContext.optimizerResponses.Where(c => c.InternalRefrenceId == request.InternalRefrenceId).ToList();
                    return await OptimizerRespnoseDTO.CreateList(result);
                }

            string downloadedFile = DownloadFile(_config.Path, _config.APIUserName, _config.APIPassword, submitedProject);
            //ToDo:Add New Exception
            if (downloadedFile == string.Empty)
                return retValue;

            var optimizedResult = _processReport.ProcessReport(downloadedFile, _config.Path, request.InternalRefrenceId, submitedProject.ExternalRefrenceId);

            
            submitedProject.IsCompleted = true;
            submitedProject.OptimizationLevel = Integration.Optimization.Domain.Enums.OptimizationLevel.OptimizationEndedWithResult;
            submitedProject.OptimizedReultFileName = downloadedFile;
            _optimizerDbContext.submitedProjects.Update(submitedProject);
            _optimizerDbContext.optimizerResponses.AddRange(optimizedResult);
            _optimizerDbContext.SaveChanges();

            return await OptimizerRespnoseDTO.CreateList(optimizedResult);

        }

        private string DownloadFile(string path, string apiUserName, string apiPassword, SubmitedProject submitedProject)
        {
            string retValue = string.Empty;
            var loginResult = _c2Logix.Login(new User(apiUserName, apiPassword)).GetAwaiter().GetResult();
            var tokenResult = _c2Logix.GetAuthToken().GetAwaiter().GetResult();
            if (_c2Logix.GetOptimizationStatus(tokenResult, int.Parse(submitedProject.ExternalRefrenceId)).GetAwaiter().GetResult().Completed)
            {
                retValue = _c2Logix.DownloadCsvResult(tokenResult, int.Parse(submitedProject.ExternalRefrenceId), path, Integration.Optimization.HttpCalls.ReportType.RouteDetails).Result;
            }
            return retValue;
        }


    }
}
