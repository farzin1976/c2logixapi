﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using MediatR;

namespace Fleetmind.Services.integration.Optimization.Application.OptimizedProjects.Commands.Submit
{
    public class SubmitProjectCommand:IRequest<SubmisionResult>
    {
        public OptimizerRequestModel Request { get; set; }
    }
}
