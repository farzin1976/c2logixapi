﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Fleetmind.Services.integration.Optimization.Application.Abstractions;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.Domain.Enums;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using Fleetmind.Services.Integration.Optimization.Persistence;
using FleetMind.C2Logix.HttpCalls;
using MediatR;

namespace Fleetmind.Services.integration.Optimization.Application.OptimizedProjects.Commands.Submit
{
    public class SubmitProjectHandler : IRequestHandler<SubmitProjectCommand, SubmisionResult>
    {
        private readonly IProcessFilesFactory _processFilesFactory;
        private readonly IConfig _config;
        private readonly IFileNameFormat _fileNameFormat;
        private readonly IOptimizerClient _c2Logix;
        private readonly OptimizerDbContext _optimizerDbContext;
        public SubmitProjectHandler(IProcessFilesFactory processFilesFactory, 
                                    IConfig config,
                                    IFileNameFormat fileNameFormat,
                                    IOptimizerClient c2Logix,
                                    OptimizerDbContext optimizerDbContext)
        {
            _processFilesFactory = processFilesFactory;
            _config = config;
            _fileNameFormat = fileNameFormat;
            _c2Logix = c2Logix;
            _optimizerDbContext = optimizerDbContext;
        }
        public async Task<SubmisionResult> Handle(SubmitProjectCommand request, CancellationToken cancellationToken)
        {
            var retValue = new SubmisionResult();
            SubmitCSV(request.Request, retValue);
            SubmitToOptimizer(request.Request, retValue);
            SubmitToDatabase(request.Request, retValue); 
            return  retValue;
        }

       private SubmisionResult SubmitToOptimizer(OptimizerRequestModel optimizeRoute, SubmisionResult submisionResult)
        {
            try
            {
                var loginResult = _c2Logix.Login(new User(_config.APIUserName, _config.APIPassword)).GetAwaiter().GetResult();
                var tokenResult = _c2Logix.GetAuthToken().GetAwaiter().GetResult();
                var projectInfo = _c2Logix.CreateProject(tokenResult, $"Project_{DateTime.Now.Ticks}").GetAwaiter().GetResult();
                submisionResult.SetExternalRefrenceId(projectInfo.Project_id.ToString());
                var resultBases = _c2Logix.UploadCSV(tokenResult, projectInfo.Project_id, _config.Path, submisionResult.FacilityFileName, UploadType.Bases).GetAwaiter().GetResult();
                while (_c2Logix.GetUploadResult(tokenResult, projectInfo.Project_id, UploadType.Bases).GetAwaiter().GetResult().In_progress)
                {
                    Thread.Sleep(100);
                }
                var resultStops = _c2Logix.UploadCSV(tokenResult, projectInfo.Project_id, _config.Path, submisionResult.StopFileName, UploadType.Stops).GetAwaiter().GetResult();
                while (_c2Logix.GetUploadResult(tokenResult, projectInfo.Project_id, UploadType.Stops).GetAwaiter().GetResult().In_progress)
                {

                    Thread.Sleep(100);
                }
                var resultVehicles = _c2Logix.UploadCSV(tokenResult, projectInfo.Project_id, _config.Path, submisionResult.VehilceFileName, UploadType.Vehicles).GetAwaiter().GetResult();
                while (_c2Logix.GetUploadResult(tokenResult, projectInfo.Project_id, UploadType.Vehicles).GetAwaiter().GetResult().In_progress)
                {
                    Thread.Sleep(100);
                }
                //Start Routing
                _c2Logix.StartRouting(tokenResult, projectInfo.Project_id);
            }
            catch (Exception ex)
            {
                submisionResult.optimizationLevel = OptimizationLevel.OptimizationEndedWithError;
                throw ex;
            }
            submisionResult.optimizationLevel = OptimizationLevel.UploadToOptimizer;
            return submisionResult;
        }
       private SubmisionResult SubmitCSV(OptimizerRequestModel optimizeRoute, SubmisionResult submisionResult)
        {

            

            try
            {
                submisionResult.FacilityFileName = _processFilesFactory.ProcessFacilities(_config.Path).SaveFile(optimizeRoute.Facilities);
                submisionResult.StopFileName = _processFilesFactory.ProcessStops(_config.Path).SaveFile(optimizeRoute.Stops);
                submisionResult.VehilceFileName = _processFilesFactory.ProcessVehicles(_config.Path).SaveFile(optimizeRoute.Vehicles);
                submisionResult.optimizationLevel = OptimizationLevel.JsonConvertedToCSV;
            }
            catch (Exception ex)
            {
                submisionResult.optimizationLevel = OptimizationLevel.OptimizationEndedWithError;
                throw ex;

            }
            finally
            {

            }
            return submisionResult;




        }
       private SubmisionResult SubmitToDatabase(OptimizerRequestModel optimizeRoute, SubmisionResult submisionResult)
        {
            //update stops by internal id was generated in the begining of request
            submisionResult.optimizationLevel = OptimizationLevel.SubmitRequestSavedToDatabase;
            var stops = optimizeRoute.Stops.Select(x => { x.InternalRefrenceId = submisionResult.InternalRefrenceId; return x; });
            var vehicles = optimizeRoute.Vehicles.Select(x => { x.InternalRefrenceId = submisionResult.InternalRefrenceId; return x; });
            var facilities = optimizeRoute.Facilities.Select(x => { x.InternalRefrenceId = submisionResult.InternalRefrenceId; return x; });
            _optimizerDbContext.stops.AddRange(stops);
            _optimizerDbContext.vehicles.AddRange(vehicles);
            _optimizerDbContext.facilities.AddRange(facilities);
            _optimizerDbContext.submitedProjects.Add(new SubmitedProject()
            {
                ExternalRefrenceId = submisionResult.ExternalRefrenceId,
                IsCompleted = false,
                LastRequestDate = DateTime.Now,
                OptimizationLevel = submisionResult.optimizationLevel,
                InternalRefrenceId = submisionResult.InternalRefrenceId,
                StopNumber = stops.Count(),
                FacilitiesNumber = facilities.Count(),
                VehiclesNumber = vehicles.Count(),
                SubmitDate = DateTime.Now,
                StopsFileName = submisionResult.StopFileName,
                FacilitiesFileName = submisionResult.FacilityFileName,
                VehiclesFileName = submisionResult.VehilceFileName

            });
            _optimizerDbContext.SaveChanges();

            return submisionResult;
        }

    }
}
