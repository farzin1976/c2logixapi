﻿using Fleetmind.Services.integration.Optimization.Application.Abstractions;
using Microsoft.Extensions.Configuration;
using System;

namespace Fleetmind.Services.integration.Optimization.Application.Infrastructure
{
    public class Config : IConfig
    {
        private readonly IConfiguration _configuration;
        public Config(IConfiguration configuration)
        {
            _configuration = configuration;
            var cnfigs = GetConfigs();
            Path = cnfigs.csvDirPath;
            APIUserName = cnfigs.c2LogixUser;
            APIPassword = cnfigs.c2LogixPassword;

        }
        private (string csvDirPath, string c2LogixUser, string c2LogixPassword) GetConfigs()
        {
            string csvDirPath = _configuration.GetValue<string>("Application:CSVFilePath:DirPath") ?? throw new ArgumentNullException("Application:CSVFilePath:DirPath");
            string c2LogixUser = _configuration.GetValue<string>("Application:C2LogixAuthSettings:User") ?? throw new ArgumentNullException("Application:C2LogixAuthSettings:User");
            string c2LogixPassword = _configuration.GetValue<string>("Application:C2LogixAuthSettings:Password") ?? throw new ArgumentNullException("Application:C2LogixAuthSettings:Password");
            return (csvDirPath, c2LogixUser, c2LogixPassword);
        }
        public string Path { get; }

        public string APIUserName { get; }

        public string APIPassword { get; }
    }
}
