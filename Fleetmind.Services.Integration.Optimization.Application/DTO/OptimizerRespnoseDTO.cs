﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Fleetmind.Services.integration.Optimization.Application.DTO
{
    public class OptimizerRespnoseDTO:IEntity
    {
        public int Id { get; set; }
        public string RouteName { get; set; }
        public string DayId { get; set; }
        public string VehicleName { get; set; }
        public string CustomerName { get; set; }
        public string StopPosition { get; set; }
        public string TsStopType { get; set; }
        public string TypeStops { get; set; }
        public string StopType { get; set; }
        public string StopDriveTime { get; set; }
        public string StopStartTime { get; set; }
        public string StopDriveDistance { get; set; }
        public string StopDuration { get; set; }
        public string StopElapsedDistance { get; set; }
        public string FacilityName { get; set; }
        public static Expression<Func<OptimizerResponseModel, OptimizerRespnoseDTO>> Projection
        {
            get
            {
                return optimized => new OptimizerRespnoseDTO
                {
                    RouteName = optimized.RouteName,
                    DayId = optimized.DayId,
                    VehicleName = optimized.VehicleName,
                    CustomerName = optimized.CustomerName,
                    StopPosition = optimized.StopPosition,
                    TsStopType = optimized.TsStopType,
                    StopDriveTime = optimized.StopDriveTime,
                    StopStartTime = optimized.StopStartTime,
                    StopDriveDistance = optimized.StopDriveDistance,
                    StopDuration = optimized.StopDuration,
                    StopElapsedDistance = optimized.StopElapsedDistance,
                    FacilityName = optimized.FacilityName

                };
            }
        }

        public static OptimizerRespnoseDTO Create(OptimizerResponseModel optimizedResult)
        {
            return Projection.Compile().Invoke(optimizedResult);
        }
        public static async Task<List<OptimizerRespnoseDTO>> CreateList(IEnumerable<OptimizerResponseModel> optimizedResults)
        {
            List<OptimizerRespnoseDTO> retValues = new List<OptimizerRespnoseDTO>();
            foreach (var item in optimizedResults)
            {
                retValues.Add(Projection.Compile().Invoke(item));
            }
            return  retValues;
        }
    }
}
