﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;



namespace Fleetmind.Services.Integration.Optimization.Application.Abstractions
{
    public abstract class SubmitOptimizationProject
    {

        protected SubmitOptimizationProject next;
        public SubmitOptimizationProject()
        {
            
        }
        public SubmitOptimizationProject SetNext(SubmitOptimizationProject next)
        {
            this.next = next;
            return next;
        }
        public SubmisionResult SubmitRoute(OptimizerRequestModel optimizeRoute,SubmisionResult submisionResult)
        {
            var retvalue = Submit(optimizeRoute, submisionResult);
            if(next !=null)
            {
                retvalue= next.SubmitRoute(optimizeRoute, retvalue);
            }
            return retvalue;
        }
        protected abstract SubmisionResult Submit(OptimizerRequestModel optimizeRoute,SubmisionResult submisionResult);
    }
}
