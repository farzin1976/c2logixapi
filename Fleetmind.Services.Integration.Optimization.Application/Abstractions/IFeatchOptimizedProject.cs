﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Fleetmind.Services.integration.Optimization.Application.Abstractions
{
    public interface IFeatchOptimizedProject
    {
        IEnumerable<SubmitedProject> GetSubmitedProjects();
        SubmitedProject GetSubmitedProject(string internalRefrenceId);
        IEnumerable<OptimizerResponseModel> GetOptimiezedResult(string internalRefrenceId);
        IEnumerable<OptimizerResponseModel> Process(string path,string apiUserName,string apiPassword,string internalRefrenceId);
        void ProcessAllIncompleted(string path, string apiUserName, string apiPassword);



    }
}
