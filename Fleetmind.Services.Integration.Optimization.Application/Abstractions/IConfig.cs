﻿namespace Fleetmind.Services.integration.Optimization.Application.Abstractions
{
    public interface IConfig
    {
        string Path { get;}
        string APIUserName { get;}
        string APIPassword { get;}

    }
}
