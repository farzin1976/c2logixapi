﻿using Fleetmind.Services.integration.Optimization.Application.Abstractions;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.Persistence;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using FleetMind.C2Logix.HttpCalls;
using Fleetmind.Services.Integration.Optimization.FileHelper.Implimentation;
using Microsoft.Extensions.Logging;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using System.Threading.Tasks;

namespace Fleetmind.Services.integration.Optimization.Application.FeatchOptimizedProjects
{
    public class FeatchOptimizedProject : IFeatchOptimizedProject
    {
        private readonly IFileSystem _fileSystem;
        private readonly OptimizerDbContext _optimizerDbContext;
        private readonly IOptimizerClient _c2Logix;
        private readonly ILogger _logger;
        private readonly IProcessReport<OptimizerResponseModel> _processReport;
        public FeatchOptimizedProject(IFileSystem fileSystem,
                                      OptimizerDbContext optimizerDbContext, 
                                      IOptimizerClient c2Logix,
                                      ILogger<FeatchOptimizedProject> logger, 
                                      IProcessReport<OptimizerResponseModel> processReport)
        {
            _fileSystem = fileSystem;
            _optimizerDbContext = optimizerDbContext;
            _c2Logix = c2Logix;
            _logger = logger;
            _processReport = processReport;
        }
        public SubmitedProject GetSubmitedProject(string internalRefrenceId)
        {
            return _optimizerDbContext.submitedProjects.FirstOrDefault(c => c.InternalRefrenceId == internalRefrenceId);
        }

        public IEnumerable<OptimizerResponseModel> GetOptimiezedResult(string internalRefrenceId)
        {
            return _optimizerDbContext.optimizerResponses.Where(c => c.InternalRefrenceId == internalRefrenceId);
        }
        public  IEnumerable<OptimizerResponseModel> Process(string path, string apiUserName, string apiPassword, string internalRefrenceId)
        {
            List<OptimizerResponseModel> retValue = new List<OptimizerResponseModel>();
            _logger.LogInformation($"Process Started");
            var submitedProject = GetSubmitedProject(internalRefrenceId);
            if (submitedProject == null)
                return retValue;
            if (submitedProject.IsCompleted)
                return retValue;

            string fileName=Upload(path,apiUserName, apiPassword,submitedProject);
            if (fileName == string.Empty)
               return retValue;
            
            var optimizedResult= _processReport.ProcessReport(fileName,path, internalRefrenceId, submitedProject.ExternalRefrenceId);
            retValue = optimizedResult.ToList();
            submitedProject.IsCompleted = true;
            submitedProject.OptimizationLevel = Integration.Optimization.Domain.Enums.OptimizationLevel.OptimizationEndedWithResult;
            submitedProject.OptimizedReultFileName = fileName;
            _optimizerDbContext.submitedProjects.Update(submitedProject);

            _optimizerDbContext.optimizerResponses.AddRange(retValue);
            _optimizerDbContext.SaveChanges();

            return retValue;
            
        }
        private string Upload(string path,string apiUserName, string apiPassword, SubmitedProject submitedProject)
        {
            string retValue = string.Empty;
            var loginResult = _c2Logix.Login(new User(apiUserName, apiPassword)).GetAwaiter().GetResult();
            var tokenResult = _c2Logix.GetAuthToken().GetAwaiter().GetResult();
            if(_c2Logix.GetOptimizationStatus(tokenResult,int.Parse(submitedProject.ExternalRefrenceId)).GetAwaiter().GetResult().Completed)
            {
                retValue = _c2Logix.DownloadCsvResult(tokenResult, int.Parse(submitedProject.ExternalRefrenceId), path, Integration.Optimization.HttpCalls.ReportType.RouteDetails).Result;
            }
            return retValue;
        }

        public IEnumerable<SubmitedProject> GetSubmitedProjects()
        {
            return _optimizerDbContext.submitedProjects;
        }

        public void ProcessAllIncompleted(string path, string apiUserName, string apiPassword)
        {
            var allIncompletedProjects = _optimizerDbContext.submitedProjects.Where(c => !c.IsCompleted).ToList();
            _logger.LogInformation($"{allIncompletedProjects?.Count()} Projects are in queue to be optimized");
            //ToDo: clould be Parallel
            foreach (var item in allIncompletedProjects)
            {
                _logger.LogInformation($"Project ({item.InternalRefrenceId }) sent request ");
                this.Process(path, apiUserName, apiPassword, item.InternalRefrenceId);
            }

        }
    }
}
