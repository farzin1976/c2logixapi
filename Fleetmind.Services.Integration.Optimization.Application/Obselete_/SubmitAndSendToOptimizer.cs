﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.Application.Abstractions;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using FleetMind.C2Logix.HttpCalls;
using System.Threading;
using System;

namespace Fleetmind.Services.Integration.Optimization.Application.SubmitRoutingProjects
{
    public class SubmitAndSendToOptimizer: SubmitOptimizationProject
    {
        private readonly IOptimizerClient _c2Logix;
        private readonly string _optimizerUserName;
        private readonly string _optimizerPassword;
        private readonly string _dirPath;
        
        public SubmitAndSendToOptimizer(IOptimizerClient c2Logix, string optimizerUserName, string optimizerPassword, string dirPath) 
        {
            _c2Logix = c2Logix;
            _optimizerUserName = optimizerUserName;
            _optimizerPassword = optimizerPassword;
            _dirPath = dirPath;
        }

        protected override SubmisionResult Submit(OptimizerRequestModel optimizeRoute,SubmisionResult submisionResult)
        {
            try
            {
                var loginResult = _c2Logix.Login(new User(_optimizerUserName, _optimizerPassword)).GetAwaiter().GetResult();
                var tokenResult = _c2Logix.GetAuthToken().GetAwaiter().GetResult();
                var projectInfo = _c2Logix.CreateProject(tokenResult, $"Project_{DateTime.Now.Ticks}").GetAwaiter().GetResult();
                submisionResult.SetExternalRefrenceId(projectInfo.Project_id.ToString());
                var resultBases = _c2Logix.UploadCSV(tokenResult, projectInfo.Project_id, _dirPath, submisionResult.FacilityFileName, UploadType.Bases).GetAwaiter().GetResult();
                while (_c2Logix.GetUploadResult(tokenResult, projectInfo.Project_id, UploadType.Bases).GetAwaiter().GetResult().In_progress)
                {
                    Thread.Sleep(1000);
                }
                var resultStops = _c2Logix.UploadCSV(tokenResult, projectInfo.Project_id, _dirPath, submisionResult.StopFileName, UploadType.Stops).GetAwaiter().GetResult();
                while (_c2Logix.GetUploadResult(tokenResult, projectInfo.Project_id, UploadType.Stops).GetAwaiter().GetResult().In_progress)
                {

                    Thread.Sleep(1000);
                }
                var resultVehicles = _c2Logix.UploadCSV(tokenResult, projectInfo.Project_id, _dirPath, submisionResult.VehilceFileName, UploadType.Vehicles).GetAwaiter().GetResult();
                while (_c2Logix.GetUploadResult(tokenResult, projectInfo.Project_id, UploadType.Vehicles).GetAwaiter().GetResult().In_progress)
                {
                    Thread.Sleep(1000);
                }
                //Start Routing
                _c2Logix.StartRouting(tokenResult, projectInfo.Project_id);
            }
            catch(Exception ex)
            {
                submisionResult.optimizationLevel = Domain.Enums.OptimizationLevel.OptimizationEndedWithError;
                throw ex;
            }
            submisionResult.optimizationLevel = Domain.Enums.OptimizationLevel.UploadToOptimizer;
            return submisionResult;
        }
    }
}
