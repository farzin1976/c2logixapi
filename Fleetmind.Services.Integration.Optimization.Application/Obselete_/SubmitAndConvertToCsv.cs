﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.Application.Abstractions;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using Fleetmind.Services.Integration.Optimization.FileHelper.Implimentation;
using System;
using Fleetmind.Services.Integration.Optimization.Domain.Enums;
using Microsoft.Extensions.Logging;

namespace Fleetmind.Services.Integration.Optimization.Application.SubmitRoutingProjects
{
    public class SubmitAndConvertToCsv : SubmitOptimizationProject
    {
        
        private readonly IProcessFilesFactory _processFilesFactory;
        private readonly string _dirPath;
        
        
        public SubmitAndConvertToCsv(IProcessFilesFactory processFilesFactory,string dirPath)
        {
            
            _processFilesFactory = processFilesFactory;
            _dirPath = dirPath;
        
        }
        protected override SubmisionResult Submit(OptimizerRequestModel optimizeRoute,SubmisionResult submisionResult)
        {
            
            IFileNameFormat fileNameFormat = new DateTimeFileNameFormat();
            //if previous step ended with error no need to process anymore
            //if (submisionResult.optimizationLevel == OptimizationLevel.OptimizationEndedWithError)
            //    return submisionResult;

            try
            {
                submisionResult.FacilityFileName = _processFilesFactory.ProcessFacilities(_dirPath).SaveFile(optimizeRoute.Facilities);
                submisionResult.StopFileName = _processFilesFactory.ProcessStops(_dirPath).SaveFile(optimizeRoute.Stops);
                submisionResult.VehilceFileName = _processFilesFactory.ProcessVehicles(_dirPath).SaveFile(optimizeRoute.Vehicles);
                submisionResult.optimizationLevel = OptimizationLevel.JsonConvertedToCSV;
            }
            catch (Exception ex)
            {
                submisionResult.optimizationLevel = OptimizationLevel.OptimizationEndedWithError;
                throw ex;
               
            }
            finally
            {
               
            }
            return submisionResult;




        }
    }
}
