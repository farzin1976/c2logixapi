﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.Domain.Enums;
using Fleetmind.Services.Integration.Optimization.Application.Abstractions;
using Fleetmind.Services.Integration.Optimization.Persistence;
using System.Linq;
using System;

namespace Fleetmind.Services.Integration.Optimization.Application.SubmitRoutingProjects
{
    public class SubmitAndSaveToDatabse: SubmitOptimizationProject
    {
        private readonly OptimizerDbContext _optimizerDbContext;
        public SubmitAndSaveToDatabse(OptimizerDbContext optimizerDbContext)
        {
            _optimizerDbContext = optimizerDbContext;
        }
        protected override SubmisionResult Submit(OptimizerRequestModel optimizeRoute,SubmisionResult submisionResult)
        {
            //update stops by internal id was generated in the begining of request
            submisionResult.optimizationLevel = OptimizationLevel.SubmitRequestSavedToDatabase;
            var stops = optimizeRoute.Stops.Select(x => { x.InternalRefrenceId = submisionResult.InternalRefrenceId; return x; });
            var vehicles = optimizeRoute.Vehicles.Select(x => { x.InternalRefrenceId = submisionResult.InternalRefrenceId; return x; });
            var facilities = optimizeRoute.Facilities.Select(x => { x.InternalRefrenceId = submisionResult.InternalRefrenceId; return x; });
            _optimizerDbContext.stops.AddRange(stops);
            _optimizerDbContext.vehicles.AddRange(vehicles);
            _optimizerDbContext.facilities.AddRange(facilities);
            _optimizerDbContext.submitedProjects.Add(new SubmitedProject()
            {
                ExternalRefrenceId = submisionResult.ExternalRefrenceId,
                IsCompleted = false,
                LastRequestDate = DateTime.Now,
                OptimizationLevel = submisionResult.optimizationLevel,
                InternalRefrenceId = submisionResult.InternalRefrenceId,
                StopNumber = stops.Count(),
                FacilitiesNumber = facilities.Count(),
                VehiclesNumber = vehicles.Count(),
                SubmitDate = DateTime.Now,
                StopsFileName=submisionResult.StopFileName,
                FacilitiesFileName=submisionResult.FacilityFileName,
                VehiclesFileName=submisionResult.VehilceFileName
                
            });

            _optimizerDbContext.SaveChanges();


            
            
            return submisionResult;
        }
    }
}
