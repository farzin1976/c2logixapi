﻿namespace Fleetmind.Services.Integration.Optimization.Domain.Enums
{
    public enum OptimizationLevel
    {
        SubmisionStarted=0,
        SubmitRequestSavedToDatabase = 1,
        JsonConvertedToCSV =2,
        UploadToOptimizer =3,
        OptimizationStarted =4,
        OptimizationEndedWithResult=5,
        OptimizationEndedWithError=6,
        ResultsWhereSavedInInternalDatabase=7,
        ResultWhereSentToRms=8,

    }
}
