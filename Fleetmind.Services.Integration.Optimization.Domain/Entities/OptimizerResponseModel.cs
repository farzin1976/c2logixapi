﻿namespace Fleetmind.Services.Integration.Optimization.Domain.Entities
{
    public class OptimizerResponseModel : IEntity
    {
        public int Id { get; set; }
        public string RouteName { get; set; }
        public string DayId { get; set; }
        public string VehicleName { get; set; }
        public string CustomerName { get; set; }
        public string StopPosition { get; set; }
        public string TsStopType { get; set; }
        public string TypeStops { get; set; }
        public string StopType { get; set; }
        public string StopDriveTime { get; set; }
        public string StopStartTime { get; set; }
        public string StopDriveDistance { get; set; }
        public string StopDuration { get; set; }
        public string StopElapsedDistance { get; set; }
        public string FacilityName { get; set; }
        public string InternalRefrenceId { get;set; }
        public string ExternalRefrenceId { get; set; }
    }
}
