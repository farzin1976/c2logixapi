﻿namespace Fleetmind.Services.Integration.Optimization.Domain.Entities
{
    public class Vehicle:IEntity
    {
        public Vehicle()
        {
            //Apply Defults
            this.Active = true;
            this.TimeToComplete = "8:00:00";
            this.StartTime = "8:00:00";
            this.Capacity = 50;
            this.PreferMajorRoads = true;
        }
        public int Id { get; set; }
        public string InternalRefrenceId { get; set; }
        public int RmsTruckId { get; set; }
        public bool Active { get; set; }
        public string Comment { get; set; }
        public string Name { get; set; }
        public string Starts { get; set; }
        public string Ends { get; set; }
        public string TimeToComplete { get; set; }
        public string StartTime { get; set; }
        public string OverTime { get; set; }
        public int Capacity { get; set; }
        public bool PreferMajorRoads { get; set; }

    }
}
