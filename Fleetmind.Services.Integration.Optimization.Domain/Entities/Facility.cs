﻿
namespace Fleetmind.Services.Integration.Optimization.Domain.Entities
{
    public class Facility:IEntity
    {
        public Facility()
        {
            //Apply defaults
            this.Active = true;
        }
        public int Id { get; set; }
        public string InternalRefrenceId { get; set; }
        public int RmsFacilityId { get; set; }
        public bool Active { get; set; }
        public string City { get; set; }
        public string Comment { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Name { get; set; }
        public string StateId { get; set; }
        public string Street { get; set; }
        public string ZipCode { get; set; }
        public bool IsFacility { get; set; }

    }
}
