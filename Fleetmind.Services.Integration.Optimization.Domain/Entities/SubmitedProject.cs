﻿using Fleetmind.Services.Integration.Optimization.Domain.Enums;
using System;

namespace Fleetmind.Services.Integration.Optimization.Domain.Entities
{
    public class SubmitedProject
    {
        public SubmitedProject()
        {
            OptimizationLevel = OptimizationLevel.OptimizationStarted;
            IsCompleted = false;
            IsTransfered = false;
        }
        public int Id { get; set; }
        public string ExternalRefrenceId { get; set; }
        public string InternalRefrenceId { get; set; }
        public int StopNumber { get; set; }
        public int VehiclesNumber { get; set; }
        public int FacilitiesNumber { get; set; }
        public string OptimizerResponse { get; set; }
        public DateTime SubmitDate { get; set; }
        public DateTime OptimizedDate { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsTransfered { get; set; }
        public OptimizationLevel OptimizationLevel { get; set; }
        public DateTime LastRequestDate { get; set; }
        public string StopsFileName { get; set; }
        public string VehiclesFileName { get; set; }
        public string FacilitiesFileName { get; set; }
        public string OptimizedReultFileName { get; set; }



    }
}
