﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;

namespace Fleetmind.Services.Integration.Optimization.Domain.Entities
{
    [DataContract]
    public class OptimizerRequestModel
    {
        public OptimizerRequestModel()
        {

        }
        public bool IsObjectNull()
        {
            if ((this.Stops !=null && this.Stops.Any()) && this.Facilities != null && this.Facilities.Any() && (this.Vehicles != null && this.Vehicles.Any()))
                return false;
            
            return true;
        }
        public OptimizerRequestModel(ICollection<Facility> facilities,ICollection<Stop> stops,ICollection<Vehicle> vehicles)
        {
            Facilities = facilities;
            Stops = stops;
            Vehicles = vehicles;
            
        }
        [DataMember(Name = "Stops")]
        public ICollection<Stop> Stops { get; private set; }
        [DataMember(Name = "Facilities")]
        public ICollection<Facility> Facilities { get; private set; }
        [DataMember(Name = "Vehicles")]
        public ICollection<Vehicle> Vehicles { get; private set; }
    }
}
