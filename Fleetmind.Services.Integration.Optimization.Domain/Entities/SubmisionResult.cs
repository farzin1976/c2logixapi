﻿using Fleetmind.Services.Integration.Optimization.Domain.Enums;
using System;

namespace Fleetmind.Services.Integration.Optimization.Domain.Entities
{
    public class SubmisionResult
    {
        public SubmisionResult()
        {
            optimizationLevel = OptimizationLevel.OptimizationStarted;
            InternalRefrenceId = Guid.NewGuid().ToString();
        }
        public string StopFileName { get; set; }
        public string VehilceFileName { get; set; }
        public string FacilityFileName { get; set; }
        public string RouteDetailsFileName{get;set;}
        public OptimizationLevel optimizationLevel { get; set; }
        //used for intrenal APIs to request and recive optimization results
        public string InternalRefrenceId { get; private set; }
        //Optimizer API will return key as a refrence for each optimization session C2Logix returns ProjectId
        public string ExternalRefrenceId { get; private set; }

        public void SetExternalRefrenceId(string id)
        {
            this.ExternalRefrenceId = id;
        }



    }
}
