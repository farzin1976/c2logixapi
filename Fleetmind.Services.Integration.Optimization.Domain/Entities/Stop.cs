﻿
namespace Fleetmind.Services.Integration.Optimization.Domain.Entities
{
    public class Stop:IEntity
    {
        public Stop()
        {
            //Apply defaults
            this.Quantity = 1;
        }
        public int Id { get; set; }
        public string InternalRefrenceId { get; set; }
        public int RmsStopId { get; set; }
        public bool Active { get; set; }
        public string City { get; set; }
        public string Comment { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Name { get; set; }
        public string StateId { get; set; }
        public string Street { get; set; }
        public string ZipCode { get; set; }
        public int Quantity { get; set; }
        public int OriginalSequence { get; set; }
        public int OptimizedSequence { get; set; }

    }
}
