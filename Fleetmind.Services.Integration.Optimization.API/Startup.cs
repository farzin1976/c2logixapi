﻿using Fleetmind.Services.integration.Optimization.API.Infrastructure;
using Fleetmind.Services.integration.Optimization.Application.Abstractions;
using Fleetmind.Services.integration.Optimization.Application.FeatchOptimizedProjects;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using Fleetmind.Services.Integration.Optimization.FileHelper.Implimentation;
using Fleetmind.Services.Integration.Optimization.Persistence;
using FleetMind.C2Logix.HttpCalls;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Examples;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.IO.Abstractions;

namespace FleetMind.C2Logix.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IFileNameFormat, DateTimeFileNameFormat>();
            services.AddSingleton<IFileSystem, FileSystem>();
            services.AddTransient<IProcessFilesFactory, ProcessCsvFileFactory>();
            services.AddTransient<IFeatchOptimizedProject, FeatchOptimizedProject>();
            services.AddSingleton<IProcessReport<OptimizerResponseModel>, ProcessCsvReport>();
            services.AddHostedService<TimedHostedService>();
            services.AddDbContext<OptimizerDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("OptimizerDatabase")));
            services.AddTransient<CookieHandler>();
            services.AddHttpClient<IOptimizerClient, C2LogixApiClient>(client =>
            {
                var apiUrl=Configuration.GetValue<string>("Application:C2LogixAuthSettings:APIUrl") ?? throw new ArgumentNullException("Application:C2LogixAuthSettings:APIUrl");
                client.BaseAddress = new Uri(apiUrl);
                
            }).AddHttpMessageHandler<CookieHandler>();
            services.AddMvc();
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "C2Logix API", Version = "v1" });
                c.OperationFilter<ExamplesOperationFilter>();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "C2Logix API V1");
                c.DocumentTitle = "Title Documentation";
                c.DocExpansion(DocExpansion.None);
                
            });
            app.UseMvc();
        }
    }
}
