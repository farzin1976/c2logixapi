﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FleetMind.C2Logix.HttpCalls;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;

using Newtonsoft.Json;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using System.IO.Abstractions;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Swashbuckle.AspNetCore.Examples;
using Fleetmind.Services.Integration.Optimization.API.Swagger;
using Fleetmind.Services.Integration.Optimization.Persistence;
using Fleetmind.Services.integration.Optimization.Application;
using Fleetmind.Services.integration.Optimization.Application.FeatchOptimizedProjects;
using Microsoft.AspNetCore.Http;
using Fleetmind.Services.integration.Optimization.Application.Abstractions;

namespace FleetMind.C2Logix.API.Controllers
{
    [Route("api/V1.0/RouteOptimizer")]
    [ApiController]
    
    public class SubmitRoutesController : Controller
    {

        private readonly IOptimizerClient _c2Logix;
        private readonly IProcessFilesFactory _processFilesFactory;
        private readonly IFileNameFormat _fileNameFormat;
        private readonly IFileSystem _fileSystem;
        private readonly ILogger<SubmitRoutesController> _logger;
        private readonly IConfiguration _configuration;
        private readonly IFeatchOptimizedProject _featchOptimizedProject;
        private readonly OptimizerDbContext _optimizerDbContext;



        public SubmitRoutesController(IOptimizerClient c2Logix,
                                      IProcessFilesFactory processFilesFactory, 
                                      IFileNameFormat fileNameFormat,
                                      IFileSystem fileSystem,
                                      ILogger<SubmitRoutesController> logger,
                                      IConfiguration configuration,
                                      IFeatchOptimizedProject featchOptimizedProject,
                                      OptimizerDbContext optimizerDbContext)
        {
             
            _c2Logix = c2Logix??throw new ArgumentNullException(nameof(c2Logix));
            _processFilesFactory = processFilesFactory??throw new ArgumentNullException(nameof(processFilesFactory));
            _fileNameFormat = fileNameFormat??throw new ArgumentNullException(nameof(fileNameFormat));
            _fileSystem = fileSystem??throw new ArgumentNullException(nameof(fileSystem));
            _logger = logger??throw new ArgumentNullException(nameof(logger));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _optimizerDbContext = optimizerDbContext?? throw new ArgumentNullException(nameof(optimizerDbContext));
            _featchOptimizedProject = featchOptimizedProject ?? throw new ArgumentNullException(nameof(featchOptimizedProject));


        }

        [HttpGet("GetSubmitedProjects")]
        [ProducesResponseType(typeof(SubmitedProject), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public IActionResult GetSubmitedProjects()
        {
            var submitedProjects = _featchOptimizedProject.GetSubmitedProjects();
            if(submitedProjects==null)
            {
                return NotFound();
            }
            return Ok(submitedProjects);
        }

        [HttpGet("GetOptimizedResult")]
        [ProducesResponseType(typeof(OptimizerResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public IActionResult GetOptimizedResult(string id)
        {
            if(string.IsNullOrEmpty(id))
            {
                return BadRequest("equest is null");
            }
            
            var configs = GetConfigs();
            
            var submitedProject = _featchOptimizedProject.GetSubmitedProject(id);
            if (submitedProject==null)
            {
                return NotFound();
            }
            if(submitedProject.IsCompleted)
            {
                return Ok(_featchOptimizedProject.GetOptimiezedResult(id));
            }
            
            var result= _featchOptimizedProject.Process(configs.csvDirPath, configs.c2LogixUser, configs.c2LogixPassword, id);
            return Ok(result);
        }

        [HttpPost("SubmitNewRoute")]
        [SwaggerRequestExample(typeof(OptimizerRequestModel), typeof(SubmitRouteModelExample))]
        [ProducesResponseType(typeof(SubmisionResult),StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Post([FromBody]OptimizerRequestModel optimizeRoute)
        {
            
            if(optimizeRoute.IsObjectNull())
            {
                return BadRequest("Request is null");
            }
            if(!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            
            try
            {
                var configs = GetConfigs();

                var submistionResult = new SubmisionResult();
                //Chain of responsibility pattern : not sure it is usefull , the idea was sequnses of events  should't be changed 

                SubmitAndConvertToCsv submitAndSaveToCSV = new SubmitAndConvertToCsv(_processFilesFactory, configs.csvDirPath);
                SubmitAndSendToOptimizer submitAndSendToOptimizer = new SubmitAndSendToOptimizer(_c2Logix, configs.c2LogixUser, configs.c2LogixPassword, configs.csvDirPath);
                SubmitAndSaveToDatabse submitAndSaveToDatabse = new SubmitAndSaveToDatabse(_optimizerDbContext);

                submitAndSaveToCSV.SetNext(submitAndSendToOptimizer);
                submitAndSendToOptimizer.SetNext(submitAndSaveToDatabse);
                submitAndSaveToCSV.SubmitRoute(optimizeRoute, submistionResult);

                //ToDo :should be async 
                return Ok(submistionResult);

            }
            catch (Exception ex)
            {

                _logger.LogError($"Optimization request terminated {ex.StackTrace}");
                return StatusCode(500, "Internal server error");
            }
            
            
        }

        #region Private Methods
        private (string csvDirPath, string c2LogixUser, string c2LogixPassword) GetConfigs()
        {
            string csvDirPath = _configuration.GetValue<string>("Application:CSVFilePath:DirPath") ?? throw new ArgumentNullException("Application:CSVFilePath:DirPath");
            string c2LogixUser = _configuration.GetValue<string>("Application:C2LogixAuthSettings:User") ?? throw new ArgumentNullException("Application:C2LogixAuthSettings:User");
            string c2LogixPassword = _configuration.GetValue<string>("Application:C2LogixAuthSettings:Password") ?? throw new ArgumentNullException("Application:C2LogixAuthSettings:Password");
            return (csvDirPath, c2LogixUser, c2LogixPassword);
        }
        #endregion




    }
}
