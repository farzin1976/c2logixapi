﻿using Fleetmind.Services.Integration.Optimization.HttpCalls;
using FleetMind.C2Logix.HttpCalls;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics;
using System.Threading;

namespace FleetMind.C2Logix.ConsoleUI
{
    class Program
    {
        
        static void Main(string[] args)
        {
            #region Init
            IServiceCollection services = new ServiceCollection();
            Startup startup = new Startup();
            startup.ConfigureServices(services);
            IServiceProvider serviceProvider = services.BuildServiceProvider();
            var _c2Logix = serviceProvider.GetService<IOptimizerClient>();
            #endregion
                        
            var loginResult = _c2Logix.Login(new User("Fleetmind", "123abc")).GetAwaiter().GetResult();
            var tokenResult = _c2Logix.GetAuthToken().GetAwaiter().GetResult();
            Console.Write($"Auth Token is :");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(tokenResult.Authentication_token);
            Console.ResetColor();
            
            //#region Delete Old Projecs
            //Console.WriteLine("Your saved Projects");

            //var projectList = _c2Logix.GetSavedProjects(tokenResult).GetAwaiter().GetResult();
            //foreach (var item in projectList)
            //{
            //    Console.Write("Project Info: ");
            //    Console.ForegroundColor = ConsoleColor.Red;
            //    Console.WriteLine($"{item.Name} , {item.Id}, {item.CreatedDate}");
            //    _c2Logix.DeleteProject(tokenResult, item.Id).GetAwaiter().GetResult();
            //    Console.ResetColor();
            //}
            //#endregion
            
            var projectInfo = _c2Logix.CreateProject(tokenResult, "Project_Created_By_API").GetAwaiter().GetResult();
            Console.Write("Project [Project_Created_By_API] is created ProjectId is:");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(projectInfo.Project_id);
            Console.ResetColor();

            Console.WriteLine("uplad Bases....");
            var resultBases = _c2Logix.UploadCSV(tokenResult, projectInfo.Project_id, @"C:\C2Logixs_API", "API_Basis.csv", UploadType.Bases).GetAwaiter().GetResult();
            using (var progress = new ProgressBar())
            {
                int i = 0;
                while (_c2Logix.GetUploadResult(tokenResult, projectInfo.Project_id, UploadType.Bases).GetAwaiter().GetResult().In_progress)
                {
                    i++;
                    progress.Report((double)i / 100);
                    Thread.Sleep(1000);
                }
            }
            Console.WriteLine("uplad stops....");
            var resultStops = _c2Logix.UploadCSV(tokenResult, projectInfo.Project_id, @"C:\C2Logixs_API", "API_Stops.csv", UploadType.Stops).GetAwaiter().GetResult();
            using (var progress = new ProgressBar())
            {
                int i = 0;
                while (_c2Logix.GetUploadResult(tokenResult, projectInfo.Project_id, UploadType.Stops).GetAwaiter().GetResult().In_progress)
                {
                    i++;
                    progress.Report((double)i / 100);
                    Thread.Sleep(1000);
                }
            }
            Console.WriteLine("upload vehicles....");
            var resultVehicles = _c2Logix.UploadCSV(tokenResult, projectInfo.Project_id, @"C:\C2Logixs_API", "API_Vehilces-new1.csv", UploadType.Vehicles).GetAwaiter().GetResult();
            using (var progress = new ProgressBar())
            {
                int i = 0;
                while (_c2Logix.GetUploadResult(tokenResult, projectInfo.Project_id, UploadType.Vehicles).GetAwaiter().GetResult().In_progress)
                {
                    i++;
                    progress.Report((double)i / 100);
                    Thread.Sleep(1000);
                }
            }

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Console.WriteLine("routing started....");

            var result = _c2Logix.StartRouting(tokenResult, projectInfo.Project_id);

            using (var progress = new ProgressBar())
            {
                int i = 0;
                while (!_c2Logix.GetOptimizationStatus(tokenResult, projectInfo.Project_id).GetAwaiter().GetResult().Completed)
                {
                    i++;
                    progress.Report((double)i / 100);
                    Thread.Sleep(1000);
                }
            }

            stopwatch.Stop();
            Console.WriteLine($"routing ended in {stopwatch.ElapsedMilliseconds / 1000} Seconds");

            string downloadPath = @"C:\C2Logixs_API";
            _c2Logix.DownloadResult(tokenResult, projectInfo.Project_id, downloadPath).GetAwaiter().GetResult();
            _c2Logix.DownloadCsvResult(tokenResult, projectInfo.Project_id, downloadPath, ReportType.RouteDetails);
            Console.Write("You can find results in this folder:");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(downloadPath);
                        
            //_c2Logix.DeleteProject(tokenResult, projectInfo.Project_id).GetAwaiter().GetResult();

            Console.ReadLine();
        }

     
    }
}
