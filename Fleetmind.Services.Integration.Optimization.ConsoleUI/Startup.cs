﻿using FleetMind.C2Logix.HttpCalls;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace FleetMind.C2Logix.ConsoleUI
{
    public class Startup
    {
        IConfigurationRoot Configuration { get; }
        public Startup()
        {
          
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<CookieHandler>();
            services.AddHttpClient<IOptimizerClient, C2LogixApiClient>(client =>
            {
                client.BaseAddress = new Uri("https://apidev.c2routeapp.com/");

            }).AddHttpMessageHandler<CookieHandler>();
        }

    }
}
