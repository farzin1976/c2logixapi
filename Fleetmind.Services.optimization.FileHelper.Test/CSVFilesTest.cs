using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.FileHelper.Implimentation;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO.Abstractions.TestingHelpers;
using System.Text.RegularExpressions;

namespace Tests
{
    public class CSVFilesTest
    {
        static string STOP_HEADER = "active,city,comment,latitude,longitude,name,state_id,street,zipcode,parameter_type_stops,parameter_customer_revenue,parameter_characteristics_stops,parameter_all_characteristics_stops,parameter_exclude_resources_stops,parameter_service_side,parameter_fixed_visit_duration,parameter_fixed_duration_per_ticket,parameter_unloading_duration_per_unit,parameter_punctuality_stops,parameter_whole_visit_in_time_window_stops,parameter_delay_penalty_per_hour_stops,parameter_frequency_stops,parameter_possible_visit_days_1,parameter_time_wdw_1_start,parameter_time_wdw_1_end,parameter_possible_visit_days_2,parameter_time_wdw_2_start,parameter_time_wdw_2_end,parameter_possible_visit_days_3,parameter_time_wdw_3_start,parameter_time_wdw_3_end,parameter_min_partial_duration_stops,parameter_min_duration_stops,parameter_quantity,parameter_quantity_2,parameter_quantity_3,parameter_quantity_4,parameter_quantity_5,parameter_quantity_6,parameter_courier_cost_stops,parameter_assign_resources,parameter_user_data1,parameter_user_data2,parameter_user_data3,parameter_user_data4,parameter_user_data5,parameter_user_data6";
        static string STOP_FIRSTROW = "TRUE,Montreal,,45.5136560,-73.5826830,7966 12TH AV,QC,,,,,,,,,,,,,,,,,,,,,,,,,,,1,,,,,,,,,,,,,";
        static string VEHICLE_HEADER = "active,comment,name,total_drive_distance,total_drive_time,facilities,starts,ends,parameter_tomtom_uid,parameter_characteristics_resource,parameter_open_start,parameter_open_stop,parameter_load_before_departure,parameter_load_on_return_resource,parameter_fixed_loading_duration,parameter_loading_duration_per_unit,parameter_distance_cost_2_resource,parameter_time_to_complete,parameter_start_time,parameter_working_days_resource,parameter_optimum_start_time_resource,parameter_briefing_duration_resource,parameter_debriefing_duration_resource,parameter_lunch_time_resource,parameter_lunch_duration_resource,parameter_overtime_hours,parameter_overtime_cost,parameter_vehicle_capacity,parameter_capacity_2_resource,parameter_capacity_3_resource,parameter_capacity_4_resource,parameter_capacity_5_resource,parameter_capacity_6_resource,parameter_capacity_7_resource,parameter_capacity_2_resource,parameter_speed_adjustment_resource,parameter_prefer_major_roads,parameter_hourly_cost_resource,parameter_fixed_cost_per_visit_resource,parameter_fixed_cost_of_use_resource,parameter_fixed_cost_of_use_resource,parameter_pay_whole_day,parameter_distance_cost_resource,parameter_distance_cost_threshold_2_resource,parameter_distance_cost_threshold_3_resource,parameter_overnight_driving_threshold_resource,parameter_max_nights_out_per_journey_resource,parameter_overnight_cost_resource,parameter_legal_max_driving_resource,parameter_legal_min_rest_duration_resource,parameter_user_data1,parameter_user_data2,parameter_user_data3,parameter_user_data4,parameter_user_data5,parameter_user_data6";
        static string VEHICLE_FIRSTROW = "TRUE,Truck1,Truck1,,,,Yard1,Yard2,,,,,,,,,,21:00,08:00,,,,,,,,,50,,,,,,,,,FALSE,,,,,,,,,,,,,,,,,,,";
        static string FACILITY_HEADER = "active,city,comment,house_num,is_facility,latitude,longitude,name,state_id,street,zipcode,parameter_depot_days_open_1,parameter_depot_days_open_2,parameter_depot_days_open_3,parameter_depot_days_open_4,parameter_depot_time_wdw_1_start,parameter_depot_time_wdw_1_end,parameter_depot_time_wdw_2_start,parameter_depot_time_wdw_2_end,parameter_depot_time_wdw_3_start,parameter_depot_time_wdw_3_end,parameter_depot_time_wdw_4_start,parameter_depot_time_wdw_4_end,parameter_user_data1,parameter_user_data2,parameter_user_data3,parameter_user_data4,parameter_user_data5,parameter_user_data6";
        static string FACILITY_FIRTSTROW = "TRUE,Montreal,,,FALSE,45.5136560,-73.5826830,FAC1,QA,AV,H2AV,,,,,,,,,,,,,,,,,,";

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestStopCSVFileFormat()
        {

            //Arrange
            MockFileSystem fileSystem = new MockFileSystem();
            var roootDirectory = fileSystem.Directory.CreateDirectory(@"C:\");
            
            SimpleFileFormat fileFormat = new SimpleFileFormat();
                       

            List<Stop> list = new List<Stop>()
            {
                new Stop(){City="Montreal",Active=true,Latitude="45.5136560",Longitude="-73.5826830",Name="7966 12TH AV",StateId="QC",Quantity=1},
            };

            var processFile = new ProcessCsvFileFactory(fileFormat, fileSystem).ProcessStops(@"C:\OptimizerTest");
            processFile.SaveFile(list);

            var fileData= fileSystem.GetFile(@"C:\OptimizerTest\Stop.CSV");
            var txtResult= fileData.TextContents;
            var result = Regex.Split(txtResult, "\r\n|\r|\n");

            Assert.AreEqual(result[0], STOP_HEADER);
            Assert.AreEqual(result[1], STOP_FIRSTROW);


        }

        [Test]
        public void TestVehcleCSVFileFormat()
        {

            //Arrange
            MockFileSystem filesystem = new MockFileSystem();
            var roootDirectory = filesystem.Directory.CreateDirectory(@"C:\");
            SimpleFileFormat fileFormat = new SimpleFileFormat();


            List<Vehicle> list = new List<Vehicle>()
            {
                new Vehicle(){Active=true,Capacity=50,Name="Truck1",Comment="Truck1",StartTime="08:00",TimeToComplete="21:00",Starts="Yard1",Ends="Yard2",PreferMajorRoads=false}
            };

            var processFile = new ProcessCsvFileFactory(fileFormat, filesystem).ProcessVehicles(@"C:\OptimizerTest");
            processFile.SaveFile(list);

            var fileData = filesystem.GetFile(@"C:\OptimizerTest\Vehicle.CSV");
            var txtResult = fileData.TextContents;
            var result = Regex.Split(txtResult, "\r\n|\r|\n");

            Assert.AreEqual(result[0], VEHICLE_HEADER);
            Assert.AreEqual(result[1], VEHICLE_FIRSTROW);
        }

        [Test]
        public void TestFacilityCSVFileFormat()
        {

            //Arrange
            MockFileSystem fileSystem = new MockFileSystem();
            var roootDirectory = fileSystem.Directory.CreateDirectory(@"C:\");
            SimpleFileFormat fileFormat = new SimpleFileFormat();


            List<Facility> list = new List<Facility>()
            {
                new Facility(){Active=true,Name="FAC1",City="Montreal",Comment="Comment",IsFacility=false,Latitude="45.5136560",Longitude="-73.5826830",StateId="QA",Street="AV",ZipCode="H2AV"}
            };


            var processFile = new ProcessCsvFileFactory(fileFormat, fileSystem).ProcessFacilities(@"C:\OptimizerTest");
            processFile.SaveFile(list);

            var fileData = fileSystem.GetFile(@"C:\OptimizerTest\Facility.CSV");
            var txtResult = fileData.TextContents;
            
            var result = Regex.Split(txtResult, "\r\n|\r|\n");

            Assert.AreEqual(result[0], FACILITY_HEADER);
            Assert.AreEqual(result[1], FACILITY_FIRTSTROW);


            
        }
    }
}