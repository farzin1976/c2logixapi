﻿

namespace Fleetmind.Services.Integration.Optimization.HttpCalls.Constants
{
    public static class StopsCSVHeaderConstants
    {
        public const string Sheetname = "sheetname";
        public const string Sheetname_Value = "Stop";
        public const string ChashedAs = "chashed_as";
        public const string ChashedAs_Value = "Stop";
        public const string FileType = "[mapping][file_type]";
        public const string FileType_Value = "csv";
        public const string EolType = "[mapping][eol_type_form_safe]";
        public const string EolType_Value = "[13,10]";
        public const string HasHeader = "[mapping][has_header]";
        public const string HasHeader_Value = "1";
        public const string Delimiter = "[mapping][delimiter]";
        public const string Delimiter_Value = ",";
        public const string Active = "[mapping][rev][active]";
        public const string Active_Value = "parameter_Customer_active";
        public const string City = "[mapping][rev][city]";
        public const string City_Value = "parameter_Customer_city";
        public const string Comment = "[mapping][rev][comment]";
        public const string Comment_Value = "parameter_Customer_comment";
        public const string Latitude = "[mapping][rev][latitude]";
        public const string Latitude_Value = "parameter_Customer_latitude";
        public const string Longitude = "[mapping][rev][longitude]";
        public const string Longitude_Value = "parameter_Customer_longitude";
        public const string CustomerName = "[mapping][rev][name]";
        public const string CustomerName_Value = "parameter_Customer_name";
        public const string StateId = "[mapping][rev][state_id]";
        public const string StateId_Value = "parameter_Customer_state_id";
        public const string Street = "[mapping][rev][street]";
        public const string Street_Value = "parameter_Customer_street";
        public const string ZopCode = "[mapping][rev][zipcode]";
        public const string ZipCode_Value = "parameter_Customer_zipcode";
        public const string Type = "[mapping][rev][parameter_type_stops]";
        public const string Type_Value = "parameter_type_stops";
        public const string Revenue = "[mapping][rev][parameter_customer_revenue]";
        public const string Revenue_Value = "parameter_customer_revenue";
        public const string Characteristic = "[mapping][rev][parameter_characteristics_stops]";
        public const string Characteristic_Value = "parameter_characteristics_stops";
        public const string AllCharacteristic = "[mapping][rev][parameter_all_characteristics_stops]";
        public const string AllCharacteristic_Value = "parameter_all_characteristics_stops";
        public const string Exclude = "[mapping][rev][parameter_exclude_resources_stops]";
        public const string Exclude_Value = "parameter_exclude_resources_stops";
        public const string ServiceSide = "[mapping][rev][parameter_service_side]";
        public const string ServiceSide_Value = "parameter_service_side";
        public const string FixedVisitDuartion = "[mapping][rev][parameter_fixed_visit_duration]";
        public const string FixedVisitDuartion_Value = "parameter_fixed_visit_duration";
        public const string FixedDurationPerTicket = "[mapping][rev][parameter_fixed_duration_per_ticket]";
        public const string FixedDurationPerTicket_Value = "parameter_fixed_duration_per_ticket";
        public const string UnloadingDurationPerUnit = "[mapping][rev][parameter_unloading_duration_per_unit]";
        public const string UnloadingDurationPerUnit_Value = "parameter_unloading_duration_per_unit";
        public const string Punctuality = "[mapping][rev][parameter_punctuality_stops]";
        public const string Punctuality_Value = "parameter_punctuality_stops";
        public const string WholeVisitInTimeWindow = "[mapping][rev][parameter_whole_visit_in_time_window_stops]";
        public const string WholeVisitInTimeWindow_Value = "parameter_whole_visit_in_time_window_stops";
        public const string PenaltyPerHour = "[mapping][rev][parameter_delay_penalty_per_hour_stops]";
        public const string PenaltyPerHour_Value = "parameter_delay_penalty_per_hour_stops";
        public const string Frequency = "[mapping][rev][parameter_frequency_stops]";
        public const string Frequency_Value = "parameter_frequency_stops";
        public const string PossibleVisitDays1 = "[mapping][rev][parameter_possible_visit_days_1]";
        public const string PossibleVisitDays1_Value = "parameter_possible_visit_days_1";
        public const string TimeWindowStart1 = "[mapping][rev][parameter_time_wdw_1_start]";
        public const string TimeWindowStart1_Value = "parameter_time_wdw_1_start";
        public const string TimeWindowEnd1 = "[mapping][rev][parameter_time_wdw_1_end]";
        public const string TimeWindowEnd1_Value = "parameter_time_wdw_1_end";
        public const string PossibleVisitDays2 = "[mapping][rev][parameter_possible_visit_days_2]";
        public const string PossibleVisitDays2_Value = "parameter_possible_visit_days_2";
        public const string TimeWindowStart2 = "[mapping][rev][parameter_time_wdw_2_start]";
        public const string TimeWindowStart2_Value = "parameter_time_wdw_2_start";
        public const string TimeWindowEnd2 = "[mapping][rev][parameter_time_wdw_2_end]";
        public const string TimeWindowEnd2_Value = "parameter_time_wdw_2_end";
        public const string PossibleVisitDays3 = "[mapping][rev][parameter_possible_visit_days_3]";
        public const string PossibleVisitDays3_Value = "parameter_possible_visit_days_3";
        public const string TimeWindowStart3 = "[mapping][rev][parameter_time_wdw_3_start]";
        public const string TimeWindowStart3_Value = "parameter_time_wdw_3_start";
        public const string TimeWindowEnd3 = "[mapping][rev][parameter_time_wdw_3_end]";
        public const string TimeWindowEnd3_Value = "parameter_time_wdw_3_end";
        public const string MinPartialDuration = "[mapping][rev][parameter_min_partial_duration_stops]";
        public const string MinPartialDuration_Value = "parameter_min_partial_duration_stops";
        public const string MinDuration = "[mapping][rev][parameter_min_duration_stops]";
        public const string MinDuration_Value = "parameter_min_duration_stops";
        public const string Quantity = "[mapping][rev][parameter_quantity]";
        public const string Quantity_Value = "parameter_quantity";
        public const string Quantity2 = "[mapping][rev][parameter_quantity_2]";
        public const string Quantity2_Value = "parameter_quantity_2";
        public const string Quantity3 = "[mapping][rev][parameter_quantity_3]";
        public const string Quantity3_Value = "parameter_quantity_3";
        public const string Quantity4 = "[mapping][rev][parameter_quantity_4]";
        public const string Quantity4_Value = "parameter_quantity_4";
        public const string Quantity5 = "[mapping][rev][parameter_quantity_5]";
        public const string Quantity5_Value = "parameter_quantity_5";
        public const string Quantity6 = "[mapping][rev][parameter_quantity_6]";
        public const string Quantity6_Value = "parameter_quantity_6";
        public const string Quantity7 = "[mapping][rev][parameter_quantity_7]";
        public const string Quantity7_Value = "parameter_quantity_7";
        public const string Quantity8 = "[mapping][rev][parameter_quantity_8]";
        public const string Quantity8_Value = "parameter_quantity_8";
        public const string Cost = "[mapping][rev][parameter_courier_cost_stops]";
        public const string Cost_Value = "parameter_courier_cost_stops";
        public const string Resources = "[mapping][rev][parameter_assign_resources]";
        public const string Resources_Value = "parameter_assign_resources";
        public const string UserData1 = "[mapping][rev][parameter_user_data1]";
        public const string UserData1_Value = "parameter_user_data1";
        public const string UserData2 = "[mapping][rev][parameter_user_data2]";
        public const string UserData2_Value = "parameter_user_data2";
        public const string UserData3 = "[mapping][rev][parameter_user_data3]";
        public const string UserData3_Value = "parameter_user_data3";
        public const string UserData4 = "[mapping][rev][parameter_user_data4]";
        public const string UserData4_Value = "parameter_user_data4";
        public const string UserData5 = "[mapping][rev][parameter_user_data5]";
        public const string UserData5_Value = "parameter_user_data5";
        public const string UserData6 = "[mapping][rev][parameter_user_data6]";
        public const string UserData6_Value = "parameter_user_data6";









    }
}
