﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FleetMind.C2Logix.HttpCalls
{
    public static class RequestConstants
    {
        public const string LOGIN = "session.json";
        public const string CREATE_PROJECT = "projects.json";
        public const string AUTH_TOKEN = "projects/new.json";
        public const string APLICARION_JSON = "application/json";
        public const string UPLOAD_START = "upload_start.json";
        public const string UPLOAD_RESULT = "upload_status.json";
        public const string OPTIMIZATION_STATUS = "query_optimization_status.json";
        public const string PROJECTS = "projects";
        public const string START_ROUTE = "startroute.json";
        public const string SAVED_BY_ID = "save_by_id";
        public const string PROJECT_REPORT = "project_report";

    }
}
