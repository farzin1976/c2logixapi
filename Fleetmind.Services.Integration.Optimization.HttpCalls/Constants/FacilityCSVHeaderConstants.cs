﻿

namespace Fleetmind.Services.Integration.Optimization.HttpCalls.Constants
{
    public static class FacilityCSVHeaderConstants
    {
        public const string Sheetname = "sheetname";
        public const string Sheetname_Value = "Base";
        public const string ChashedAs = "chashed_as";
        public const string ChashedAs_Value = "Base";
        public const string FileType = "[mapping][file_type]";
        public const string FileType_Value = "csv";
        public const string EolType = "[mapping][eol_type_form_safe]";
        public const string EolType_Value = "[13,10]";
        public const string HasHeader = "[mapping][has_header]";
        public const string HasHeader_Value = "1";
        public const string Delimiter = "[mapping][delimiter]";
        public const string Delimiter_Value = ",";
        public const string Active = "[mapping][rev][active]";
        public const string Active_Value = "parameter_Facility_active";
        public const string City = "[mapping][rev][city]";
        public const string City_Value = "parameter_Facility_city";
        public const string Comment = "[mapping][rev][comment]";
        public const string Comment_Value = "parameter_Facility_comment";
        public const string HouseNumber = "[mapping][rev][house_num]";
        public const string HouseNumber_Value = "parameter_Facility_house_num";
        public const string IsFacility = "[mapping][rev][is_facility]";
        public const string IsFacility_Value = "parameter_Facility_is_facility";
        public const string Latitude = "[mapping][rev][latitude]";
        public const string Latitude_Value = "parameter_Facility_latitude";
        public const string Longitude = "[mapping][rev][longitude]";
        public const string Longitude_Value = "parameter_Facility_longitude";
        public const string FacilityName = "[mapping][rev][name]";
        public const string Facility_Value = "parameter_Facility_name";
        public const string StateId = "[mapping][rev][state_id]";
        public const string StateId_Value = "parameter_Facility_state_id";
        public const string Street = "[mapping][rev][street]";
        public const string Street_Value = "parameter_Facility_street";
        public const string ZopCode = "[mapping][rev][zipcode]";
        public const string ZipCode_Value = "parameter_Facility_zipcode";
        public const string DaysOpe1 = "[mapping][rev][parameter_depot_days_open_1]";
        public const string DaysOpe1_Value = "parameter_depot_days_open_1";
        public const string DaysOpe2 = "[mapping][rev][parameter_depot_days_open_2]";
        public const string DaysOpe2_Value = "parameter_depot_days_open_2";
        public const string DaysOpe3 = "[mapping][rev][parameter_depot_days_open_3]";
        public const string DaysOpe3_Value = "parameter_depot_days_open_3";
        public const string DaysOpe4 = "[mapping][rev][parameter_depot_days_open_4]";
        public const string DaysOpe4_Value = "parameter_depot_days_open_4";
        public const string TimeWindowStart1 = "[mapping][rev][parameter_depot_time_wdw_1_start]";
        public const string TimeWindowStart1_Value = "parameter_depot_time_wdw_1_start";
        public const string TimeWindowEnd1 = "[mapping][rev][parameter_depot_time_wdw_1_end]";
        public const string TimeWindowEnd1_Value = "parameter_depot_time_wdw_1_end";
        public const string TimeWindowStart2 = "[mapping][rev][parameter_depot_time_wdw_2_start]";
        public const string TimeWindowStart2_Value = "parameter_depot_time_wdw_2_start";
        public const string TimeWindowEnd2 = "[mapping][rev][parameter_depot_time_wdw_2_end]";
        public const string TimeWindowEnd2_Value = "parameter_depot_time_wdw_2_end";
        public const string TimeWindowStart3 = "[mapping][rev][parameter_depot_time_wdw_3_start]";
        public const string TimeWindowStart3_Value = "parameter_depot_time_wdw_3_start";
        public const string TimeWindowEnd3 = "[mapping][rev][parameter_depot_time_wdw_3_end]";
        public const string TimeWindowEnd3_Value = "parameter_depot_time_wdw_3_end";
        public const string TimeWindowStart4 = "[mapping][rev][parameter_depot_time_wdw_4_start]";
        public const string TimeWindowStart4_Value = "parameter_depot_time_wdw_4_start";
        public const string TimeWindowEnd4 = "[mapping][rev][parameter_depot_time_wdw_4_end]";
        public const string TimeWindowEnd4_Value = "parameter_depot_time_wdw_4_end";
        public const string UserData1 = "[mapping][rev][parameter_user_data1]";
        public const string UserData1_Value = "parameter_user_data1";
        public const string UserData2 = "[mapping][rev][parameter_user_data2]";
        public const string UserData2_Value = "parameter_user_data2";
        public const string UserData3 = "[mapping][rev][parameter_user_data3]";
        public const string UserData3_Value = "parameter_user_data3";
        public const string UserData4 = "[mapping][rev][parameter_user_data4]";
        public const string UserData4_Value = "parameter_user_data4";
        public const string UserData5 = "[mapping][rev][parameter_user_data5]";
        public const string UserData5_Value = "parameter_user_data5";
        public const string UserData6 = "[mapping][rev][parameter_user_data6]";
        public const string UserData6_Value = "parameter_user_data6";

    }
}
