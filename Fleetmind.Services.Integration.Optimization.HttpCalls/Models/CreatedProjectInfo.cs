﻿using Newtonsoft.Json;

namespace FleetMind.C2Logix.HttpCalls
{
    public class CreatedProjectInfo
    {
        [JsonProperty("error")]
        public string Error { get; set; }
        [JsonProperty("Project_id")]
        public int Project_id { get; set; }
    }
}
