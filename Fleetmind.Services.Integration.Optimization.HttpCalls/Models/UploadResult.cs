﻿using Newtonsoft.Json;

namespace FleetMind.C2Logix.HttpCalls
{
    public class UploadResult
    {
        [JsonProperty("in_progress")]
        public bool In_progress { get; set; }
    }
}
