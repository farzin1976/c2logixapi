﻿
using Newtonsoft.Json;

namespace FleetMind.C2Logix.HttpCalls
{
    public class AuthToken
    {
        [JsonProperty("authentication_token")]
        public string Authentication_token { get; set; }
        
    }
}
