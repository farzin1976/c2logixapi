﻿using Newtonsoft.Json;

namespace FleetMind.C2Logix.HttpCalls
{
    public class User
    {
        public User(string login,string password)
        {
            Login = login;
            Password = password;

        }
        [JsonProperty("login")]
        public string Login { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
