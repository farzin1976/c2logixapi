﻿using Newtonsoft.Json;
namespace FleetMind.C2Logix.HttpCalls
{
    public class RoutingCompletion
    {
        [JsonProperty("completed")]
        public bool Completed { get; set; }
    }
}
