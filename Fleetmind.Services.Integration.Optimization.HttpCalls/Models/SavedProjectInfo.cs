﻿using Newtonsoft.Json;
using System;

namespace FleetMind.C2Logix.HttpCalls
{
    public class SavedProjectInfo
    {
        [JsonProperty("created_at")]
        public DateTime CreatedDate { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
