﻿using Fleetmind.Services.Integration.Optimization.HttpCalls.Constants;

namespace Fleetmind.Services.Integration.Optimization.HttpCalls.Models
{
    public class StopCsvDto:ICsvDTO
    {
        [HttpHeader(StopsCSVHeaderConstants.Sheetname, StopsCSVHeaderConstants.Sheetname_Value)]
        public string Sheetname { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.ChashedAs, StopsCSVHeaderConstants.ChashedAs_Value)]
        public string CashedAs { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.FileType, StopsCSVHeaderConstants.FileType_Value)]
        public string FileType { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.EolType, StopsCSVHeaderConstants.EolType_Value)]
        public string EolType { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.HasHeader, StopsCSVHeaderConstants.HasHeader)]
        public string HasHeader { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Delimiter, StopsCSVHeaderConstants.Delimiter_Value)]
        public string Delimiter { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Active, StopsCSVHeaderConstants.Active_Value)]
        public string Active { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.City, StopsCSVHeaderConstants.City_Value)]
        public string City { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Comment, StopsCSVHeaderConstants.Comment_Value)]
        public string Comment { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Latitude, StopsCSVHeaderConstants.Latitude_Value)]
        public string Latitude { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Longitude, StopsCSVHeaderConstants.Longitude_Value)]
        public string Longitude { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.CustomerName, StopsCSVHeaderConstants.CustomerName_Value)]
        public string Name { get; set; }
        //[HttpHeader("[mapping][rev][split_delivery_factor]", " ")]
        //public string SplitDeleviryFactor { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.StateId, StopsCSVHeaderConstants.StateId_Value)]
        public string StateId { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Street, StopsCSVHeaderConstants.Street_Value)]
        public string Street { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.ZopCode, StopsCSVHeaderConstants.ZipCode_Value)]
        public string ZipCode { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Type, StopsCSVHeaderConstants.Type_Value)]
        public string Type { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Revenue, StopsCSVHeaderConstants.Revenue_Value)]
        public string Revenue { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Characteristic, StopsCSVHeaderConstants.ChashedAs_Value)]
        public string Characteristic { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.AllCharacteristic, StopsCSVHeaderConstants.AllCharacteristic)]
        public string AllCharacteristic { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Exclude, StopsCSVHeaderConstants.Exclude_Value)]
        public string Exclude { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.ServiceSide, StopsCSVHeaderConstants.ServiceSide_Value)]
        public string ServiceSide { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.FixedVisitDuartion, StopsCSVHeaderConstants.FixedVisitDuartion_Value)]
        public string FixedVisitDuartion { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.FixedDurationPerTicket, StopsCSVHeaderConstants.FixedDurationPerTicket_Value)]
        public string FixedDurationPerTicket { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.UnloadingDurationPerUnit, StopsCSVHeaderConstants.UnloadingDurationPerUnit_Value)]
        public string UnloadingDurationPerUnit { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Punctuality, StopsCSVHeaderConstants.Punctuality_Value)]
        public string Punctuality { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.WholeVisitInTimeWindow, StopsCSVHeaderConstants.WholeVisitInTimeWindow_Value)]
        public string WholeVisitInTimeWindow { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.PenaltyPerHour, StopsCSVHeaderConstants.PenaltyPerHour_Value)]
        public string PenaltyPerHour { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Frequency, StopsCSVHeaderConstants.Frequency_Value)]
        public string Frequency { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.PossibleVisitDays1, StopsCSVHeaderConstants.PossibleVisitDays1_Value)]
        public string PossibleVisitDays1 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.TimeWindowStart1, StopsCSVHeaderConstants.TimeWindowStart1_Value)]
        public string TimeWindowStart1 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.TimeWindowEnd1, StopsCSVHeaderConstants.TimeWindowEnd1_Value)]
        public string TimeWindowEnd1 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.PossibleVisitDays2, StopsCSVHeaderConstants.PossibleVisitDays2_Value)]
        public string PossibleVisitDays2 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.TimeWindowStart2, StopsCSVHeaderConstants.TimeWindowStart2_Value)]
        public string TimeWindowStart2 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.TimeWindowEnd2, StopsCSVHeaderConstants.TimeWindowEnd2_Value)]
        public string TimeWindowEnd2 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.PossibleVisitDays3, StopsCSVHeaderConstants.PossibleVisitDays3_Value)]
        public string PossibleVisitDays3 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.TimeWindowStart3, StopsCSVHeaderConstants.TimeWindowStart3_Value)]
        public string TimeWindowStart3 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.TimeWindowEnd3, StopsCSVHeaderConstants.TimeWindowEnd2_Value)]
        public string TimeWindowEnd3 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.MinPartialDuration, StopsCSVHeaderConstants.MinPartialDuration_Value)]
        public string MinPartialDuration { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.MinPartialDuration, StopsCSVHeaderConstants.MinPartialDuration_Value)]
        public string MinDuration { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Quantity, StopsCSVHeaderConstants.Quantity_Value)]
        public string Quantity { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Quantity2, StopsCSVHeaderConstants.Quantity2_Value)]
        public string Quantity2 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Quantity3, StopsCSVHeaderConstants.Quantity3_Value)]
        public string Quantity3 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Quantity4, StopsCSVHeaderConstants.Quantity4_Value)]
        public string Quantity4 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Quantity5, StopsCSVHeaderConstants.Quantity5_Value)]
        public string Quantity5 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Quantity6, StopsCSVHeaderConstants.Quantity6_Value)]
        public string Quantity6 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Quantity7, StopsCSVHeaderConstants.Quantity7_Value)]
        public string Quantity7 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Quantity8, StopsCSVHeaderConstants.Quantity8_Value)]
        public string Quantity8 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Cost, StopsCSVHeaderConstants.Cost_Value)]
        public string Cost { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.Resources, StopsCSVHeaderConstants.Resources_Value)]
        public string Resources { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.UserData1, StopsCSVHeaderConstants.UserData1_Value)]
        public string UserData1 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.UserData2, StopsCSVHeaderConstants.UserData2_Value)]
        public string UserData2 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.UserData3, StopsCSVHeaderConstants.UserData3_Value)]
        public string UserData3 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.UserData4, StopsCSVHeaderConstants.UserData4_Value)]
        public string UserData4 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.UserData4, StopsCSVHeaderConstants.UserData4_Value)]
        public string UserData5 { get; set; }
        [HttpHeader(StopsCSVHeaderConstants.UserData6, StopsCSVHeaderConstants.UserData6_Value)]
        public string UserData6 { get; set; }
    }
}
