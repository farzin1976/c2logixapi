﻿namespace Fleetmind.Services.Integration.Optimization.HttpCalls.Models
{
    public class VehicleCsvDto: ICsvDTO
    {
        [HttpHeader("sheetname", "Vehicle")]
        public string Sheetname { get; set; }
        [HttpHeader("chashed_as", "Vehicle")]
        public string CashedAs { get; set; }
        [HttpHeader("[mapping][file_type]", "csv")]
        public string FileType { get; set; }
        [HttpHeader("[mapping][eol_type_form_safe]", "[13,10]")]
        public string EolType { get; set; }
        [HttpHeader("[mapping][has_header]", "1")]
        public string HasHeader { get; set; }
        [HttpHeader("[mapping][delimiter]", ",")]
        public string Delimiter { get; set; }
        [HttpHeader("[mapping][rev][active]", "parameter_Vehicle_active")]
        public string Active { get; set; }
        [HttpHeader("[mapping][rev][comment]", "parameter_Vehicle_comment")]
        public string Comment { get; set; }
        [HttpHeader("[mapping][rev][name]", "parameter_Vehicle_name")]
        public string Name { get; set; }
        [HttpHeader("[mapping][rev][total_drive_distance]", "total_drive_distance")]
        public string TotalDriveDistance { get; set; }
        [HttpHeader("[mapping][rev][total_drive_time]", "total_drive_time")]
        public string TotalDriveTime { get; set; }
        [HttpHeader("[mapping][rev][facilities]", "parameter_Vehicle_facilities")]
        public string Facilities { get; set; }
        [HttpHeader("[mapping][rev][starts]", "parameter_Vehicle_starts")]
        public string Starts { get; set; }
        [HttpHeader("[mapping][rev][ends]", "parameter_Vehicle_ends")]
        public string Ends { get; set; }
        [HttpHeader("[mapping][rev][parameter_tomtom_uid]", "parameter_tomtom_uid")]
        public string TomTomUid { get; set; }
        [HttpHeader("[mapping][rev][parameter_characteristics_resource]", "parameter_characteristics_resource")]
        public string CharacteristicsResource { get; set; }
        [HttpHeader("[mapping][rev][parameter_open_start]", "parameter_open_start")]
        public string OpenStart { get; set; }
        [HttpHeader("[mapping][rev][parameter_open_stop]", "parameter_open_stop")]
        public string OpenStop { get; set; }
        [HttpHeader("[mapping][rev][parameter_load_before_departure]", "parameter_load_before_departure")]
        public string LoadBeforDepurture { get; set; }
        [HttpHeader("[mapping][rev][parameter_load_on_return_resource]", "parameter_load_on_return_resource")]
        public string LoadOnReturn { get; set; }
        [HttpHeader("[mapping][rev][parameter_fixed_loading_duration]", "parameter_fixed_loading_duration")]
        public string FixedLoadingDuration { get; set; }
        [HttpHeader("[mapping][rev][parameter_loading_duration_per_unit]", "parameter_loading_duration_per_unit")]
        public string LoadingDurationPerUnit { get; set; }
        [HttpHeader("[mapping][rev][parameter_distance_cost_2_resource]", "parameter_distance_cost_2_resource")]
        public string DistanceCost2 { get; set; }
        [HttpHeader("[mapping][rev][parameter_time_to_complete]", "parameter_time_to_complete")]
        public string TimeToComplete { get; set; }
        [HttpHeader("[mapping][rev][parameter_start_time]", "parameter_start_time")]
        public string StartTime { get; set; }
        [HttpHeader("[mapping][rev][parameter_working_days_resource]", "parameter_working_days_resource")]
        public string WorkingDays { get; set; }
        [HttpHeader("[mapping][rev][parameter_optimum_start_time_resource]", "parameter_optimum_start_time_resource")]
        public string OptimalStartTime { get; set; }
        [HttpHeader("[mapping][rev][parameter_briefing_duration_resource]", "parameter_briefing_duration_resource")]
        public string BrifingDuration { get; set; }
        [HttpHeader("[mapping][rev][parameter_debriefing_duration_resource]", "parameter_debriefing_duration_resource")]
        public string DeBrifingDuration { get; set; }
        [HttpHeader("[mapping][rev][parameter_lunch_time_resource]", "parameter_lunch_time_resource")]
        public string LunchTime { get; set; }
        [HttpHeader("[mapping][rev][parameter_lunch_duration_resource]", "parameter_lunch_duration_resource")]
        public string LuncDuration { get; set; }
        [HttpHeader("[mapping][rev][parameter_overtime_hours]", "parameter_overtime_hours")]
        public string OverTimeHoures { get; set; }
        [HttpHeader("[mapping][rev][parameter_overtime_cost]", "parameter_overtime_cost")]
        public string OverTimeCost { get; set; }
        [HttpHeader("[mapping][rev][parameter_vehicle_capacity]", "parameter_vehicle_capacity")]
        public string Capacity { get; set; }
        [HttpHeader("[mapping][rev][parameter_capacity_2_resource]", "parameter_capacity_2_resource")]
        public string Capacity2 { get; set; }
        [HttpHeader("[mapping][rev][parameter_capacity_3_resource]", "parameter_capacity_3_resource")]
        public string Capacity3 { get; set; }
        [HttpHeader("[mapping][rev][parameter_capacity_4_resource]", "parameter_capacity_4_resource")]
        public string Capacity4 { get; set; }
        [HttpHeader("[mapping][rev][parameter_capacity_5_resource]", "parameter_capacity_5_resource")]
        public string Capacity5 { get; set; }
        [HttpHeader("[mapping][rev][parameter_capacity_6_resource]", "parameter_capacity_6_resource")]
        public string Capacity6 { get; set; }
        [HttpHeader("[mapping][rev][parameter_capacity_7_resource]", "parameter_capacity_7_resource")]
        public string Capacity7 { get; set; }
        [HttpHeader("[mapping][rev][parameter_capacity_8_resource]", "parameter_capacity_8_resource")]
        public string Capacity8 { get; set; }
        [HttpHeader("[mapping][rev][parameter_speed_adjustment_resource]", "parameter_speed_adjustment_resource")]
        public string SpeedAdjustment { get; set; }
        [HttpHeader("[mapping][rev][parameter_prefer_major_roads]", "parameter_prefer_major_roads")]
        public string MajorRoads { get; set; }
        [HttpHeader("[mapping][rev][parameter_hourly_cost_resource]", "parameter_hourly_cost_resource")]
        public string HourlyCost { get; set; }
        [HttpHeader("[mapping][rev][parameter_fixed_cost_per_visit_resource]", "parameter_fixed_cost_per_visit_resource")]
        public string CostPerVisit { get; set; }
        [HttpHeader("[mapping][rev][parameter_fixed_cost_of_use_resource]", "parameter_fixed_cost_of_use_resource")]
        public string CostOfUse { get; set; }
        [HttpHeader("[mapping][rev][parameter_non_use_penalty]", "parameter_non_use_penalty")]
        public string NonUsePenalty { get; set; }
        [HttpHeader("[mapping][rev][parameter_pay_whole_day]", "parameter_pay_whole_day")]
        public string PayWholeDay { get; set; }
        [HttpHeader("[mapping][rev][parameter_distance_cost_resource]", "parameter_distance_cost_resource")]
        public string DistanceCost { get; set; }
        [HttpHeader("[mapping][rev][parameter_distance_cost_threshold_2_resource]", "parameter_distance_cost_threshold_2_resource")]
        public string DistanceCostThreshold2 { get; set; }
        [HttpHeader("[mapping][rev][parameter_distance_cost_3_resource]", "parameter_distance_cost_3_resource")]
        public string DistanceCost3 { get; set; }
        [HttpHeader("[mapping][rev][parameter_distance_cost_threshold_3_resource]", "parameter_distance_cost_threshold_3_resource")]
        public string DistanceCostThreshold3 { get; set; }
        [HttpHeader("[mapping][rev][parameter_overnight_driving_threshold_resource]", "parameter_overnight_driving_threshold_resource")]
        public string OverNightDrivingThreshold { get; set; }
        [HttpHeader("[mapping][rev][parameter_max_nights_out_per_journey_resource]", "parameter_max_nights_out_per_journey_resource")]
        public string MaxNightOutPerJurny { get; set; }
        [HttpHeader("[mapping][rev][parameter_overnight_cost_resource]", "parameter_overnight_cost_resource")]
        public string OverNightCost { get; set; }
        [HttpHeader("[mapping][rev][parameter_legal_max_driving_resource]", "parameter_legal_max_driving_resource")]
        public string LegalMaxDriving { get; set; }
        [HttpHeader("[mapping][rev][parameter_legal_min_rest_duration_resource]", "parameter_legal_min_rest_duration_resource")]
        public string LegalMinRestDuration { get; set; }
        [HttpHeader("[mapping][rev][parameter_legal_max_daily_driving_resource]", "parameter_legal_max_daily_driving_resource")]
        public string LegalMaxRestDuration { get; set; }
        [HttpHeader("[mapping][rev][parameter_legal_night_rest_duration_resource]", "parameter_legal_night_rest_duration_resource")]
        public string LegalNightRestDuration { get; set; }
        [HttpHeader("[mapping][rev][parameter_user_data1]", "parameter_user_data1")]
        public string UserData1 { get; set; }
        [HttpHeader("[mapping][rev][parameter_user_data2]", "parameter_user_data2")]
        public string UserData2 { get; set; }
        [HttpHeader("[mapping][rev][parameter_user_data3]", "parameter_user_data3")]
        public string UserData3 { get; set; }
        [HttpHeader("[mapping][rev][parameter_user_data4]", "parameter_user_data4")]
        public string UserData4 { get; set; }
        [HttpHeader("[mapping][rev][parameter_user_data5]", "parameter_user_data5")]
        public string UserData5 { get; set; }
        [HttpHeader("[mapping][rev][parameter_user_data6]", "parameter_user_data6")]
        public string UserData6 { get; set; }
    }
}
