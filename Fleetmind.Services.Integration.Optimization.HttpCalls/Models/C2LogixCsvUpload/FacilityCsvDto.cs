﻿namespace Fleetmind.Services.Integration.Optimization.HttpCalls.Models
{
    public class FacilityCsvDto:ICsvDTO
    {
        [HttpHeader("sheetname", "Base")]
        public string Sheetname { get; set; }
        [HttpHeader("chashed_as", "Base")]
        public string CashedAs { get; set; }
        [HttpHeader("[mapping][file_type]", "csv")]
        public string FileType { get; set; }
        [HttpHeader("[mapping][eol_type_form_safe]", "[13,10]")]
        public string EolType { get; set; }
        [HttpHeader("[mapping][has_header]","1")]
        public string HasHeader { get; set; }
        [HttpHeader("[mapping][delimiter]",",")]
        public string Delimiter { get; set; }
        [HttpHeader("[mapping][rev][active]", "parameter_Facility_active")]
        public string Active { get; set; }
        [HttpHeader("[mapping][rev][city]", "parameter_Facility_city")]
        public string City { get; set; }
        [HttpHeader("[mapping][rev][comment]", "parameter_Facility_comment")]
        public string Comment { get; set; }
        [HttpHeader("[mapping][rev][house_num]", "parameter_Facility_house_num")]
        public string HouseNumber { get; set; }
        [HttpHeader("[mapping][rev][is_facility]", "parameter_Facility_is_facility")]
        public string IsFacility { get; set; }
        [HttpHeader("[mapping][rev][latitude]", "parameter_Facility_latitude")]
        public string Latitude { get; set; }
        [HttpHeader("[mapping][rev][longitude]", "parameter_Facility_longitude")]
        public string Longitude { get; set; }
        [HttpHeader("[mapping][rev][name]", "parameter_Facility_name")]
        public string Name { get; set; }
        [HttpHeader("[mapping][rev][state_id]", "parameter_Facility_state_id")]
        public string StateId { get; set; }
        [HttpHeader("[mapping][rev][street]", "parameter_Facility_street")]
        public string Street { get; set; }
        [HttpHeader("[mapping][rev][zipcode]", "parameter_Facility_zipcode")]
        public string ZipCode { get; set; }
        [HttpHeader("[mapping][rev][parameter_depot_days_open_1]", "parameter_depot_days_open_1")]
        public string DaysOpe1 { get; set; }
        [HttpHeader("[mapping][rev][parameter_depot_days_open_2]", "parameter_depot_days_open_2")]
        public string DaysOpe2 { get; set; }
        [HttpHeader("[mapping][rev][parameter_depot_days_open_3]", "parameter_depot_days_open_3")]
        public string DaysOpe3 { get; set; }
        [HttpHeader("[mapping][rev][parameter_depot_days_open_4]", "parameter_depot_days_open_4")]
        public string DaysOpe4 { get; set; }
        [HttpHeader("[mapping][rev][parameter_depot_time_wdw_1_start]", "parameter_depot_time_wdw_1_start")]
        public string WindowStart1 { get; set; }
        [HttpHeader("[mapping][rev][parameter_depot_time_wdw_1_end]", "parameter_depot_time_wdw_1_end")]
        public string WindowEnd1 { get; set; }
        [HttpHeader("[mapping][rev][parameter_depot_time_wdw_2_start]", "parameter_depot_time_wdw_2_start")]
        public string WindowStart2 { get; set; }
        [HttpHeader("[mapping][rev][parameter_depot_time_wdw_2_end]", "parameter_depot_time_wdw_2_end")]
        public string WindowEnd2 { get; set; }
        [HttpHeader("[mapping][rev][parameter_depot_time_wdw_3_start]", "parameter_depot_time_wdw_3_start")]
        public string WindowStart3 { get; set; }
        [HttpHeader("[mapping][rev][parameter_depot_time_wdw_3_end]", "parameter_depot_time_wdw_3_end")]
        public string WindowEnd3 { get; set; }
        [HttpHeader("[mapping][rev][parameter_depot_time_wdw_4_start]", "parameter_depot_time_wdw_4_start")]
        public string WindowStart4 { get; set; }
        [HttpHeader("[mapping][rev][parameter_depot_time_wdw_4_end]", "parameter_depot_time_wdw_4_end")]
        public string WindowEnd4 { get; set; }
        [HttpHeader("[mapping][rev][parameter_user_data1]", "parameter_user_data1")]
        public string UserData1 { get; set; }
        [HttpHeader("[mapping][rev][parameter_user_data2]", "parameter_user_data2")]
        public string UserData2 { get; set; }
        [HttpHeader("[mapping][rev][parameter_user_data3]", "parameter_user_data3")]
        public string UserData3 { get; set; }
        [HttpHeader("[mapping][rev][parameter_user_data4]", "parameter_user_data4")]
        public string UserData4 { get; set; }
        [HttpHeader("[mapping][rev][parameter_user_data5]", "parameter_user_data5")]
        public string UserData5 { get; set; }
        [HttpHeader("[mapping][rev][parameter_user_data6]", "parameter_user_data6")]
        public string UserData6 { get; set; }

        
    }
}
