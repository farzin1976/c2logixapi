﻿namespace Fleetmind.Services.Integration.Optimization.HttpCalls.Models
{
    public class HttpHeaderModel
    {
        public string HeaderName { get;  private set; }
        public string HeaderValue { get;  private set; }
        public HttpHeaderModel(string name,string value)
        {
            HeaderName = name;
            HeaderValue = value;
        }

    }
}
