﻿using Fleetmind.Services.Integration.Optimization.HttpCalls;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FleetMind.C2Logix.HttpCalls
{
    public interface IOptimizerClient
    {
        
        Task<bool> Login(User user);
        Task<AuthToken> GetAuthToken();
        Task<CreatedProjectInfo> CreateProject(AuthToken authToken,string projectName);
        Task<IEnumerable<SavedProjectInfo>> GetSavedProjects(AuthToken authToken);
        Task<bool> DeleteProject(AuthToken authToken, int projectId);
        Task<string> UploadExcel(AuthToken authToken, int projectId, string uploadPath, string uploadFileName, UploadType uploadType);
        Task<string> UploadCSV(AuthToken authToken, int projectId, string uploadPath, string uploadFileName, UploadType uploadType);
        Task<UploadResult> GetUploadResult(AuthToken authToken, int projectId, UploadType uploadType);
        Task<RoutingCompletion> GetOptimizationStatus(AuthToken authToken, int projectId);
        Task<string> StartRouting(AuthToken authToken, int projectId);
        Task DownloadResult(AuthToken authToken, int projectId, string downloadPath);
        Task<string> DownloadCsvResult(AuthToken authToken, int projectId, string downloadPath, ReportType reportType);
        

    }
}
