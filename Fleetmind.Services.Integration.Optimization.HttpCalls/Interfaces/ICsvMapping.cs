﻿using Fleetmind.Services.Integration.Optimization.HttpCalls.Interfaces;
using Fleetmind.Services.Integration.Optimization.HttpCalls.Utils;
using FleetMind.C2Logix.HttpCalls;
using System;
using System.Net.Http;

namespace Fleetmind.Services.Integration.Optimization.HttpCalls
{
    public interface ICsvMapping
    {
        void AddCsvHeaders(MultipartFormDataContent content, UploadType uploadType);
    }

   
    
}
