﻿using Fleetmind.Services.Integration.Optimization.HttpCalls.Models;
using FleetMind.C2Logix.HttpCalls;
using System;

namespace Fleetmind.Services.Integration.Optimization.HttpCalls.Interfaces
{
    public interface ICsvDtoTypeFactory
    {
        Type GetCsvDto(UploadType uploadType);
    }
   
}
