﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Fleetmind.Services.Integration.Optimization.HttpCalls;

namespace FleetMind.C2Logix.HttpCalls
{
    public class C2LogixApiClient : IOptimizerClient
    {
        private readonly HttpClient _client;
        
        public C2LogixApiClient(HttpClient client)
        {
            _client = client;
            
            
        }

        private void SetHeaders()
        {
            _client.DefaultRequestHeaders.Add("Accept-Language", "en-US,en;q=0.9");
            _client.DefaultRequestHeaders.Add("Connection", "keep-alive");
            _client.DefaultRequestHeaders.Add("Cache-Control", "no-cache");
            _client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, br");
            
        }
       

        public async Task<CreatedProjectInfo> CreateProject(AuthToken authToken,string projectName)
        {
            CreatedProjectInfo projectInfo = new CreatedProjectInfo();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            SetHeaders();

            var response = await _client.PostAsync(RequestConstants.CREATE_PROJECT, new FormUrlEncodedContent(new[] {
               new KeyValuePair<string, string>("authenticity_token", authToken.Authentication_token),
               new KeyValuePair<string, string>("project[name]", projectName)}));
            if (response.IsSuccessStatusCode)
            {
                projectInfo = JsonConvert.DeserializeObject<CreatedProjectInfo>(await response.Content.ReadAsStringAsync());
                

            }
            return projectInfo;



        }

        public async Task<AuthToken> GetAuthToken()
        {
            AuthToken authToken = new AuthToken();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(RequestConstants.APLICARION_JSON));
            
            HttpResponseMessage response = await _client.GetAsync(RequestConstants.AUTH_TOKEN);
            if (response.IsSuccessStatusCode)
            {
                authToken = JsonConvert.DeserializeObject<AuthToken>(response.Content.ReadAsStringAsync().Result);
                

            }
            return authToken;

        }

        public async Task<IEnumerable<SavedProjectInfo>> GetSavedProjects(AuthToken authToken)
        {
            List<SavedProjectInfo> result = new List<SavedProjectInfo>();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            SetHeaders();

            var response = await _client.GetAsync(RequestConstants.CREATE_PROJECT);
            if (response.IsSuccessStatusCode)
            {

                result = JsonConvert.DeserializeObject<List<SavedProjectInfo>>(response.Content.ReadAsStringAsync().Result);

            }
            return result;
        }

        public async Task<bool> Login(User user)
        {
            
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(RequestConstants.APLICARION_JSON));
            var json = JsonConvert.SerializeObject(user);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, RequestConstants.APLICARION_JSON);
            HttpResponseMessage response = await _client.PostAsync(
              RequestConstants.LOGIN, stringContent);
            
            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
            
        }

        public async Task<bool> DeleteProject(AuthToken authToken, int projectId)
        {
            var result = false;
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            SetHeaders();

            var response = await _client.PostAsync($"project/{projectId}/delete.json", new FormUrlEncodedContent(new[] {
               new KeyValuePair<string, string>("authenticity_token", authToken.Authentication_token)}));
            if (response.IsSuccessStatusCode)
            {
                result = true;

            }
                return result;
        }

        public async Task<string> UploadExcel(AuthToken authToken, int projectId, string uploadPath, string uploadFileName, UploadType uploadType)
        {
            string result = "";
            SetHeaders();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("multipart/form-data"));
            string filePath = $@"{uploadPath}\{uploadFileName}";
            using (var content = new MultipartFormDataContent())
            {
                var stream = new StreamContent(File.Open(filePath, FileMode.Open));
                stream.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");


                stream.Headers.Add("Content-Disposition", $"form-data; name=\"{uploadType.ToDescription()}[uploaded_data]\"; filename=\"{uploadFileName}\"");


                content.Add(stream, $"{uploadType.ToDescription()}[uploaded_data]", uploadFileName);
                content.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data") { };


                var authContent = new StringContent(authToken.Authentication_token);
                authContent.Headers.Add("Content-Disposition", "form-data; name=\"authenticity_token\"");
                content.Add(authContent, "authenticity_token");

                var projectIdContent = new StringContent(projectId.ToString());
                projectIdContent.Headers.Add("Content-Disposition", "form-data; name=\"project_id\"");
                content.Add(projectIdContent, "project_id");
                string endpoint = $"{uploadType.ToDescription()}/{RequestConstants.UPLOAD_START}";
               

                var response = await _client.PostAsync(endpoint, content);
                result =await response.Content.ReadAsStringAsync();
                

            }

            return result;
        }

        public async Task<UploadResult> GetUploadResult(AuthToken authToken, int projectId, UploadType uploadType)
        {
            UploadResult uploadResult = new UploadResult();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            SetHeaders();
            var response = await _client.GetAsync($"{uploadType.ToDescription()}/{RequestConstants.UPLOAD_RESULT}?project_id={projectId}");
            if (response.IsSuccessStatusCode)
            {
                 uploadResult = JsonConvert.DeserializeObject<UploadResult>(response.Content.ReadAsStringAsync().Result);
                
            }
            return uploadResult;


        }

        public async Task<RoutingCompletion> GetOptimizationStatus(AuthToken authToken, int projectId)
        {
            RoutingCompletion routingCompletion = new RoutingCompletion();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            SetHeaders();
            var response = await _client.GetAsync($"{RequestConstants.PROJECTS}/{projectId}/{RequestConstants.OPTIMIZATION_STATUS}");
            if (response.IsSuccessStatusCode)
            {
                 routingCompletion = JsonConvert.DeserializeObject<RoutingCompletion>(response.Content.ReadAsStringAsync().Result);
                
            }
            return routingCompletion;
        }

        public  async Task<string> StartRouting(AuthToken authToken, int projectId)
        {
            string result = "";
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            SetHeaders();

            var response =await _client.PostAsync($"/{RequestConstants.PROJECTS}/{projectId}/{RequestConstants.START_ROUTE}?route_type=nearly", new FormUrlEncodedContent(new[] {
               new KeyValuePair<string, string>("authenticity_token", authToken.Authentication_token)}));

            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsStringAsync();

            }
            return result;

        }

        public async Task DownloadResult(AuthToken authToken, int projectId, string downloadPath)
        {
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            SetHeaders();
            var response = await _client.GetAsync($"{RequestConstants.PROJECTS}/{projectId}/{RequestConstants.SAVED_BY_ID}");
            if (response.IsSuccessStatusCode)
            {

                // Read from response and write to file
                FileStream fileStream = File.Create($@"{downloadPath}\routingresult_{projectId}_{DateTime.Now.ToLongDateString()}.xls");
                var result =await response.Content.ReadAsByteArrayAsync();
                fileStream.Write(result);
                fileStream.Close();
                // end while
            }

        }

        public async Task<string> UploadCSV(AuthToken authToken, int projectId, string uploadPath, string uploadFileName, UploadType uploadType)
        {
            string result = "";
            SetHeaders();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("multipart/form-data"));
            string filePath = $@"{uploadPath}\{uploadFileName}";
            using (var content = new MultipartFormDataContent())
            {
                var stream = new StreamContent(File.Open(filePath, FileMode.Open));
                stream.Headers.ContentType = new MediaTypeHeaderValue("application/xml");


                stream.Headers.Add("Content-Disposition", $"form-data; name=\"{uploadType.ToDescription()}[uploaded_data]\"; filename=\"{uploadFileName}\"");


                content.Add(stream, $"{uploadType.ToDescription()}[uploaded_data]", uploadFileName);
                content.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data") { };


                var authContent = new StringContent(authToken.Authentication_token);
                authContent.Headers.Add("Content-Disposition", "form-data; name=\"authenticity_token\"");
                content.Add(authContent, "authenticity_token");

                var projectIdContent = new StringContent(projectId.ToString());
                projectIdContent.Headers.Add("Content-Disposition", "form-data; name=\"project_id\"");
                content.Add(projectIdContent, "project_id");
                string endpoint = $"{uploadType.ToDescription()}/{RequestConstants.UPLOAD_START}";

                new CsvMapping().AddCsvHeaders(content,uploadType);

                var response = await _client.PostAsync(endpoint, content);
                result = await response.Content.ReadAsStringAsync();


            }

            return result;
        }

        public async Task<string> DownloadCsvResult(AuthToken authToken, int projectId, string downloadPath, ReportType reportType)
        {
            string downloadFileName = "";
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            SetHeaders();
            _client.DefaultRequestHeaders.Add("Referer", $"{_client.BaseAddress}/{ RequestConstants.PROJECTS}/{ projectId}");
            var response = await _client.GetAsync($"{RequestConstants.PROJECT_REPORT}/{reportType.ToDescription()}");
            if (response.IsSuccessStatusCode)
            {

                // Read from response and write to file
                downloadFileName = $"routingresult_{projectId}_{reportType.ToDescription()}_{DateTime.Now.Ticks}.csv";
                string fullPath = $@"{downloadPath}\{downloadFileName}";
                FileStream fileStream = File.Create(fullPath);
                var result = await response.Content.ReadAsByteArrayAsync();
                fileStream.Write(result);
                fileStream.Close();
                // end while
            }
            return downloadFileName;
        }
    }
}
