﻿using Fleetmind.Services.Integration.Optimization.HttpCalls.Interfaces;
using Fleetmind.Services.Integration.Optimization.HttpCalls.Models;
using FleetMind.C2Logix.HttpCalls;
using System;

namespace Fleetmind.Services.Integration.Optimization.HttpCalls
{
    public class CsvMappingFactory : ICsvDtoTypeFactory
    {
        public Type GetCsvDto(UploadType uploadType)
        {
            switch (uploadType)
            {
                case UploadType.Vehicles:
                    return typeof(VehicleCsvDto);
                case UploadType.Stops:
                    return typeof(StopCsvDto);
                case UploadType.Bases:
                    return typeof(FacilityCsvDto);
                default:
                    return typeof(FacilityCsvDto);

            }
        }
    }
}
