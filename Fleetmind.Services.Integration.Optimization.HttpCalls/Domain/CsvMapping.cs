﻿using Fleetmind.Services.Integration.Optimization.HttpCalls.Utils;
using FleetMind.C2Logix.HttpCalls;
using System;
using System.Net.Http;

namespace Fleetmind.Services.Integration.Optimization.HttpCalls
{
    public class CsvMapping : ICsvMapping
    {
        public void AddCsvHeaders(MultipartFormDataContent content, UploadType uploadType)
        {
            Type type = new CsvMappingFactory().GetCsvDto(uploadType);
            foreach (var item in ReadCutomeHttpHeaderAttributes.LoopThroughAllFields(type))
            {
                var httpContetnt = new StringContent(item.HeaderValue);
                httpContetnt.Headers.Add("Content-Disposition", $"form-data; name=\"{item.HeaderName}\"");
                content.Add(httpContetnt, item.HeaderValue);
            }


        }
    }
}
