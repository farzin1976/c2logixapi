﻿
using System.ComponentModel;

namespace FleetMind.C2Logix.HttpCalls
{
    public enum UploadType
    {
        [Description("vehicles")]
        Vehicles =1,
        [Description("customers")]
        Stops =2,
        [Description("facilities")]
        Bases =3
    }
}
