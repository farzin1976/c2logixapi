﻿using System.ComponentModel;

namespace Fleetmind.Services.Integration.Optimization.HttpCalls
{
    public enum ReportType
    {
        [Description("Route_Data.csv")]
        RouteData = 1,
        [Description("Route_Detail.csv")]
        RouteDetails = 2,
        [Description("Route_Statistics.csv")]
        RouteStatistics = 3
    }
}
