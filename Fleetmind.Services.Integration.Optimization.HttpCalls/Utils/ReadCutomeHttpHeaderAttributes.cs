﻿
using Fleetmind.Services.Integration.Optimization.HttpCalls.Models;
using System.Collections.Generic;

namespace Fleetmind.Services.Integration.Optimization.HttpCalls.Utils
{
    public static class ReadCutomeHttpHeaderAttributes
    {
        public static IEnumerable<HttpHeaderModel> LoopThroughAllFields(System.Type t)
        {
            foreach (var prop in t.GetProperties())
            {
                var attrs = (HttpHeaderAttribute[])prop.GetCustomAttributes(typeof(HttpHeaderAttribute), false);
                foreach (var attr in attrs)
                {
                    string name = attr.HeaderName;
                    string valu = attr.HeaderValue;
                    yield return new HttpHeaderModel(name, valu);
                         
                }
            }
            
        }
    }
}
