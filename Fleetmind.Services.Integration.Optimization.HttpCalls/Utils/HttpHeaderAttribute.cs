﻿using System;

namespace Fleetmind.Services.Integration.Optimization.HttpCalls
{
    [AttributeUsage(AttributeTargets.Property)]
    public class HttpHeaderAttribute:Attribute
    {
        private readonly string _name;
        private readonly string _value;
        
        public HttpHeaderAttribute(string name,string value)
        {
            _name = name;
            _value = value;
            
        }
        public string HeaderName { get { return _name; } }
        public string HeaderValue { get { return _value; } }
        
    }
}
