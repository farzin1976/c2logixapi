﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.Net.Http.Headers;

namespace FleetMind.C2Logix.HttpCalls
{
    public class CookieHandler : DelegatingHandler
    {
        private readonly CookieContainer _cookies;
        public CookieHandler()
        {
            _cookies = new CookieContainer();
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            try
            {

                request.Headers.Add("Cookie", _cookies.GetCookieHeader(request.RequestUri));
                var response = await base.SendAsync(request, cancellationToken).ConfigureAwait(false);

                if (response.Headers.TryGetValues("Set-Cookie", out var newCookies))
                {
                    
                    var parsedCookies = ParseCookies(newCookies.ToList());
                    foreach (var item in parsedCookies)
                    {
                        _cookies.Add(request.RequestUri, new Cookie(item.Name, item.Value, item.Path));
                    }
                }

                return response;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private List<CookieItem> ParseCookies(IEnumerable<string> input)
        {
            List<CookieItem> result = new List<CookieItem>();
            foreach (var item in input)
            {
                var splited = item.Split(";");

                var cookieNameValue = splited.FirstOrDefault().Split("=");
                var path = splited[1].Split("=");
                result.Add(new CookieItem()
                {
                    Name = cookieNameValue.FirstOrDefault(),
                    Value = cookieNameValue[1],
                    Path = path[1]
                });

            }
            return result;
        }
        private class CookieItem
        {
            public string Name { get; set; }
            public string Value { get; set; }
            public string Path { get; set; }
        }



    }
}
