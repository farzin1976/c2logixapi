﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Fleetmind.Services.integration.Optimization.Persistence.Migrations
{
    public partial class SubmitedProjectsAddedNewFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FacilitiesFileName",
                table: "SubmitedProjects",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OptimizedReultFileName",
                table: "SubmitedProjects",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StopsFileName",
                table: "SubmitedProjects",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VehiclesFileName",
                table: "SubmitedProjects",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FacilitiesFileName",
                table: "SubmitedProjects");

            migrationBuilder.DropColumn(
                name: "OptimizedReultFileName",
                table: "SubmitedProjects");

            migrationBuilder.DropColumn(
                name: "StopsFileName",
                table: "SubmitedProjects");

            migrationBuilder.DropColumn(
                name: "VehiclesFileName",
                table: "SubmitedProjects");
        }
    }
}
