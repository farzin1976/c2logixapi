﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Fleetmind.Services.integration.Optimization.Persistence.Migrations
{
    public partial class newFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "PreferMajorRoads",
                table: "Vehicles",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsFacility",
                table: "Facilities",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PreferMajorRoads",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "IsFacility",
                table: "Facilities");
        }
    }
}
