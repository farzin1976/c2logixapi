﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Fleetmind.Services.integration.Optimization.Persistence.Migrations
{
    public partial class UpdateSubmitedProjects : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsTransfered",
                table: "SubmitedProjects",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsTransfered",
                table: "SubmitedProjects");
        }
    }
}
