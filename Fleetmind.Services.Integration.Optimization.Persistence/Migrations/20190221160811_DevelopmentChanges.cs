﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Fleetmind.Services.integration.Optimization.Persistence.Migrations
{
    public partial class DevelopmentChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "optimizeRouteResults");

            migrationBuilder.CreateTable(
                name: "Facilities",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InternalRefrenceId = table.Column<string>(nullable: false),
                    RmsFacilityId = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    Comment = table.Column<string>(nullable: true),
                    Latitude = table.Column<string>(nullable: true),
                    Longitude = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    StateId = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Facilities", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "OptimizerReportRouteDetails",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RouteName = table.Column<string>(nullable: true),
                    DayId = table.Column<string>(nullable: true),
                    VehicleName = table.Column<string>(nullable: true),
                    CustomerName = table.Column<string>(nullable: true),
                    StopPosition = table.Column<string>(nullable: true),
                    TsStopType = table.Column<string>(nullable: true),
                    TypeStops = table.Column<string>(nullable: true),
                    StopType = table.Column<string>(nullable: true),
                    StopDriveTime = table.Column<string>(nullable: true),
                    StopStartTime = table.Column<string>(nullable: true),
                    StopDriveDistance = table.Column<string>(nullable: true),
                    StopDuration = table.Column<string>(nullable: true),
                    StopElapsedDistance = table.Column<string>(nullable: true),
                    FacilityName = table.Column<string>(nullable: true),
                    InternalRefrenceId = table.Column<string>(nullable: false),
                    ExternalRefrenceId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OptimizerReportRouteDetails", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Stops",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InternalRefrenceId = table.Column<string>(nullable: false),
                    RmsStopId = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    Comment = table.Column<string>(nullable: true),
                    Latitude = table.Column<string>(nullable: true),
                    Longitude = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    StateId = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    OriginalSequence = table.Column<int>(nullable: false),
                    OptimizedSequence = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stops", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "SubmitedProjects",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExternalRefrenceId = table.Column<string>(nullable: true),
                    InternalRefrenceId = table.Column<string>(nullable: false),
                    StopNumber = table.Column<int>(nullable: false),
                    VehiclesNumber = table.Column<int>(nullable: false),
                    FacilitiesNumber = table.Column<int>(nullable: false),
                    OptimizerResponse = table.Column<string>(nullable: true),
                    SubmitDate = table.Column<DateTime>(nullable: false),
                    OptimizedDate = table.Column<DateTime>(nullable: false),
                    IsCompleted = table.Column<bool>(nullable: false),
                    OptimizationLevel = table.Column<int>(nullable: false),
                    LastRequestDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubmitedProjects", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Vehicles",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InternalRefrenceId = table.Column<string>(nullable: false),
                    RmsTruckId = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    Comment = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Starts = table.Column<string>(nullable: true),
                    Ends = table.Column<string>(nullable: true),
                    TimeToComplete = table.Column<string>(nullable: true),
                    StartTime = table.Column<string>(nullable: true),
                    OverTime = table.Column<string>(nullable: true),
                    Capacity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicles", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OptimizerReportRouteDetails_InternalRefrenceId",
                table: "OptimizerReportRouteDetails",
                column: "InternalRefrenceId");

            migrationBuilder.CreateIndex(
                name: "IX_SubmitedProjects_InternalRefrenceId",
                table: "SubmitedProjects",
                column: "InternalRefrenceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Facilities");

            migrationBuilder.DropTable(
                name: "OptimizerReportRouteDetails");

            migrationBuilder.DropTable(
                name: "Stops");

            migrationBuilder.DropTable(
                name: "SubmitedProjects");

            migrationBuilder.DropTable(
                name: "Vehicles");

            migrationBuilder.CreateTable(
                name: "optimizeRouteResults",
                columns: table => new
                {
                    OptimizeRouteResultId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CustomerName = table.Column<string>(nullable: true),
                    DayId = table.Column<string>(nullable: true),
                    ExternalRefrenceId = table.Column<string>(nullable: true),
                    FacilityName = table.Column<string>(nullable: true),
                    InternalRefrenceId = table.Column<string>(nullable: true),
                    RouteName = table.Column<string>(nullable: true),
                    StopDriveDistance = table.Column<string>(nullable: true),
                    StopDriveTime = table.Column<string>(nullable: true),
                    StopDuration = table.Column<string>(nullable: true),
                    StopElapsedDistance = table.Column<string>(nullable: true),
                    StopPosition = table.Column<string>(nullable: true),
                    StopStartTime = table.Column<string>(nullable: true),
                    StopType = table.Column<string>(nullable: true),
                    TsStopType = table.Column<string>(nullable: true),
                    TypeStops = table.Column<string>(nullable: true),
                    VehicleName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_optimizeRouteResults", x => x.OptimizeRouteResultId);
                });
        }
    }
}
