﻿// <auto-generated />
using System;
using Fleetmind.Services.Integration.Optimization.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Fleetmind.Services.integration.Optimization.Persistence.Migrations
{
    [DbContext(typeof(OptimizerDbContext))]
    [Migration("20190308203357_newFields")]
    partial class newFields
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.2-servicing-10034")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Fleetmind.Services.Integration.Optimization.Domain.Entities.Facility", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ID")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("Active");

                    b.Property<string>("City");

                    b.Property<string>("Comment");

                    b.Property<string>("InternalRefrenceId")
                        .IsRequired();

                    b.Property<bool>("IsFacility");

                    b.Property<string>("Latitude");

                    b.Property<string>("Longitude");

                    b.Property<string>("Name");

                    b.Property<int>("RmsFacilityId");

                    b.Property<string>("StateId");

                    b.Property<string>("Street");

                    b.Property<string>("ZipCode");

                    b.HasKey("Id");

                    b.ToTable("Facilities");
                });

            modelBuilder.Entity("Fleetmind.Services.Integration.Optimization.Domain.Entities.OptimizerResponseModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ID")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("CustomerName");

                    b.Property<string>("DayId");

                    b.Property<string>("ExternalRefrenceId");

                    b.Property<string>("FacilityName");

                    b.Property<string>("InternalRefrenceId")
                        .IsRequired();

                    b.Property<string>("RouteName");

                    b.Property<string>("StopDriveDistance");

                    b.Property<string>("StopDriveTime");

                    b.Property<string>("StopDuration");

                    b.Property<string>("StopElapsedDistance");

                    b.Property<string>("StopPosition");

                    b.Property<string>("StopStartTime");

                    b.Property<string>("StopType");

                    b.Property<string>("TsStopType");

                    b.Property<string>("TypeStops");

                    b.Property<string>("VehicleName");

                    b.HasKey("Id");

                    b.HasIndex("InternalRefrenceId");

                    b.ToTable("OptimizerReportRouteDetails");
                });

            modelBuilder.Entity("Fleetmind.Services.Integration.Optimization.Domain.Entities.Stop", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ID")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("Active");

                    b.Property<string>("City");

                    b.Property<string>("Comment");

                    b.Property<string>("InternalRefrenceId")
                        .IsRequired();

                    b.Property<string>("Latitude");

                    b.Property<string>("Longitude");

                    b.Property<string>("Name");

                    b.Property<int>("OptimizedSequence");

                    b.Property<int>("OriginalSequence");

                    b.Property<int>("Quantity");

                    b.Property<int>("RmsStopId");

                    b.Property<string>("StateId");

                    b.Property<string>("Street");

                    b.Property<string>("ZipCode");

                    b.HasKey("Id");

                    b.ToTable("Stops");
                });

            modelBuilder.Entity("Fleetmind.Services.Integration.Optimization.Domain.Entities.SubmitedProject", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ID")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ExternalRefrenceId");

                    b.Property<string>("FacilitiesFileName");

                    b.Property<int>("FacilitiesNumber");

                    b.Property<string>("InternalRefrenceId")
                        .IsRequired();

                    b.Property<bool>("IsCompleted");

                    b.Property<bool>("IsTransfered");

                    b.Property<DateTime>("LastRequestDate");

                    b.Property<int>("OptimizationLevel");

                    b.Property<DateTime>("OptimizedDate");

                    b.Property<string>("OptimizedReultFileName");

                    b.Property<string>("OptimizerResponse");

                    b.Property<int>("StopNumber");

                    b.Property<string>("StopsFileName");

                    b.Property<DateTime>("SubmitDate");

                    b.Property<string>("VehiclesFileName");

                    b.Property<int>("VehiclesNumber");

                    b.HasKey("Id");

                    b.HasIndex("InternalRefrenceId");

                    b.ToTable("SubmitedProjects");
                });

            modelBuilder.Entity("Fleetmind.Services.Integration.Optimization.Domain.Entities.Vehicle", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ID")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("Active");

                    b.Property<int>("Capacity");

                    b.Property<string>("Comment");

                    b.Property<string>("Ends");

                    b.Property<string>("InternalRefrenceId")
                        .IsRequired();

                    b.Property<string>("Name");

                    b.Property<string>("OverTime");

                    b.Property<bool>("PreferMajorRoads");

                    b.Property<int>("RmsTruckId");

                    b.Property<string>("StartTime");

                    b.Property<string>("Starts");

                    b.Property<string>("TimeToComplete");

                    b.HasKey("Id");

                    b.ToTable("Vehicles");
                });
#pragma warning restore 612, 618
        }
    }
}
