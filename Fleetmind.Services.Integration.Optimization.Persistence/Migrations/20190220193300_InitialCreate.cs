﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Fleetmind.Services.integration.Optimization.Persistence.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "optimizeRouteResults",
                columns: table => new
                {
                    OptimizeRouteResultId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RouteName = table.Column<string>(nullable: true),
                    DayId = table.Column<string>(nullable: true),
                    VehicleName = table.Column<string>(nullable: true),
                    CustomerName = table.Column<string>(nullable: true),
                    StopPosition = table.Column<string>(nullable: true),
                    TsStopType = table.Column<string>(nullable: true),
                    TypeStops = table.Column<string>(nullable: true),
                    StopType = table.Column<string>(nullable: true),
                    StopDriveTime = table.Column<string>(nullable: true),
                    StopStartTime = table.Column<string>(nullable: true),
                    StopDriveDistance = table.Column<string>(nullable: true),
                    StopDuration = table.Column<string>(nullable: true),
                    StopElapsedDistance = table.Column<string>(nullable: true),
                    FacilityName = table.Column<string>(nullable: true),
                    InternalRefrenceId = table.Column<string>(nullable: true),
                    ExternalRefrenceId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_optimizeRouteResults", x => x.OptimizeRouteResultId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "optimizeRouteResults");
        }
    }
}
