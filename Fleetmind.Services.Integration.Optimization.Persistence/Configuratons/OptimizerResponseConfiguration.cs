﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Fleetmind.Services.Integration.Optimization.Persistence.Configuratons
{
    public class OptimizerResponseConfiguration : IEntityTypeConfiguration<OptimizerResponseModel>
    {
        public void Configure(EntityTypeBuilder<OptimizerResponseModel> builder)
        {
            builder.ToTable("OptimizerReportRouteDetails");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.InternalRefrenceId).IsRequired();
            builder.HasIndex(p => new { p.InternalRefrenceId });

        }
    }
}
