﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Fleetmind.Services.integration.Optimization.Persistence.Configuratons
{
    public class FacilityConfiguratin : IEntityTypeConfiguration<Facility>
    {
        public void Configure(EntityTypeBuilder<Facility> builder)
        {
            builder.ToTable("Facilities");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.InternalRefrenceId).IsRequired();
        }
    }
}
