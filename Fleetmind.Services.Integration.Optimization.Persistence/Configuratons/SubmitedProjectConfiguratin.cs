﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Fleetmind.Services.integration.Optimization.Persistence.Configuratons
{
    public class SubmitedProjectConfiguratin : IEntityTypeConfiguration<SubmitedProject>
    {

        public void Configure(EntityTypeBuilder<SubmitedProject> builder)
        {
            builder.ToTable("SubmitedProjects");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.InternalRefrenceId).IsRequired();
            builder.Property(e => e.OptimizationLevel).IsRequired();
            builder.HasIndex(p => new { p.InternalRefrenceId });
        }
    }
}
