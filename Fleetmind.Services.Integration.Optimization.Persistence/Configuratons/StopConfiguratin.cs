﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Fleetmind.Services.integration.Optimization.Persistence.Configuratons
{
    public class StopConfiguratin : IEntityTypeConfiguration<Stop>
    {
        public void Configure(EntityTypeBuilder<Stop> builder)
        {
            builder.ToTable("Stops");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.InternalRefrenceId).IsRequired();

        }
    }
}
