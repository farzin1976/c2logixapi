﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Fleetmind.Services.integration.Optimization.Persistence.Configuratons
{
    public class VehicleConfiguratin : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            builder.ToTable("Vehicles");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.InternalRefrenceId).IsRequired();
        }
    }
}
