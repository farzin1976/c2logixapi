﻿using Microsoft.EntityFrameworkCore;
using Fleetmind.Services.Integration.Optimization.Persistence.Infrastructure;

namespace Fleetmind.Services.Integration.Optimization.Persistence
{
    public class OptimizerDbContextFactory : DesignTimeDbContextFactoryBase<OptimizerDbContext>
    {
        protected override OptimizerDbContext CreateNewInstance(DbContextOptions<OptimizerDbContext> options)
        {
            return new OptimizerDbContext(options);
        }
    }
}
