﻿using Microsoft.EntityFrameworkCore;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.Persistence.Extentions;


namespace Fleetmind.Services.Integration.Optimization.Persistence
{
    public class OptimizerDbContext: DbContext
    {
        public OptimizerDbContext(DbContextOptions<OptimizerDbContext> options):base(options)
        {

        }
        public DbSet<OptimizerResponseModel> optimizerResponses { get; set; }
        public DbSet<SubmitedProject> submitedProjects { get; set; }
        public DbSet<Stop> stops { get; set; }
        public DbSet<Vehicle> vehicles { get; set; }
        public DbSet<Facility> facilities { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Search all Configurations and apply them 
            modelBuilder.ApplyAllConfigurations(); 
        }
    }
}
