﻿using Fleetmind.Services.integration.Optimization.Application.Abstractions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Fleetmind.C2LogixIntegrationService.Infrastructure
{
    public class TimedHostedService: IHostedService, IDisposable
    {
        private readonly ILogger _logger;
        private Timer _timer;
        private readonly IConfiguration _configuration;
        public IServiceScopeFactory _serviceScopeFactory;
        public TimedHostedService(ILogger<TimedHostedService> logger, IConfiguration configuration, IServiceScopeFactory serviceScopeFactory)
        {
            _logger = logger;
            _configuration = configuration;
            _serviceScopeFactory = serviceScopeFactory;
        }
        public void Dispose()
        {
            _timer?.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Background Service is starting.");
            string strInterval = _configuration.GetValue<string>("Application:BackGroundTask:Interval") ?? throw new ArgumentNullException("Application:BackGroundTask:Interval");
            int intInterval = 60;
            int.TryParse(strInterval, out intInterval);
            _timer = new Timer(DoWork, null, TimeSpan.Zero,
           TimeSpan.FromSeconds(intInterval));
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            _logger.LogInformation("Timed Background Service is working.");


            //using (var scope = _serviceScopeFactory.CreateScope())
            //{
            //    var scopedProcessingService =
            //        scope.ServiceProvider
            //            .GetRequiredService<IFeatchOptimizedProject>();
            //    var configs = GetConfigs();

            //    scopedProcessingService.ProcessAllIncompleted(configs.csvDirPath, configs.c2LogixUser, configs.c2LogixPassword);
            //}
        }

        private (string csvDirPath, string c2LogixUser, string c2LogixPassword) GetConfigs()
        {
            string csvDirPath = _configuration.GetValue<string>("Application:CSVFilePath:DirPath") ?? throw new ArgumentNullException("Application:CSVFilePath:DirPath");
            string c2LogixUser = _configuration.GetValue<string>("Application:C2LogixAuthSettings:User") ?? throw new ArgumentNullException("Application:C2LogixAuthSettings:User");
            string c2LogixPassword = _configuration.GetValue<string>("Application:C2LogixAuthSettings:Password") ?? throw new ArgumentNullException("Application:C2LogixAuthSettings:Password");
            return (csvDirPath, c2LogixUser, c2LogixPassword);
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Background Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
    }
}
