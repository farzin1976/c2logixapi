using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Fleetmind.Services.integration.Optimization.Application.Abstractions;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using Fleetmind.Services.Integration.Optimization.FileHelper.Implimentation;
using Fleetmind.Services.Integration.Optimization.Persistence;
using FleetMind.C2Logix.HttpCalls;
using System.IO.Abstractions;
using System;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.Examples;
using Swashbuckle.AspNetCore.SwaggerUI;
using Fleetmind.Services.integration.Optimization.Application.Infrastructure;
using MediatR;
using MediatR.Pipeline;
using Fleetmind.Services.integration.Optimization.Application.OptimizedProjects.Queries;
using System.Reflection;
using FluentValidation.AspNetCore;

namespace Fleetmind.C2LogixIntegrationService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // Add MediatR
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPreProcessorBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPerformanceBehaviour<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
            services.AddMediatR(typeof(GetSubmitedProjectsHandler).GetTypeInfo().Assembly);
            /////////////////////
            services.AddSingleton<IConfig, Config>();
            services.AddSingleton<IFileNameFormat, DateTimeFileNameFormat>();
            services.AddSingleton<IFileSystem, FileSystem>();
            services.AddTransient<IProcessFilesFactory, ProcessCsvFileFactory>();
            services.AddSingleton<IProcessReport<OptimizerResponseModel>, ProcessCsvReport>();
            //Don't need this TimedHosedService 
            //services.AddHostedService<TimedHostedService>();
            services.AddDbContext<OptimizerDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("OptimizerDatabase")));
            services.AddTransient<CookieHandler>();
            services.AddHttpClient<IOptimizerClient, C2LogixApiClient>(client =>
            {
                var apiUrl = Configuration.GetValue<string>("Application:C2LogixAuthSettings:APIUrl") ?? throw new ArgumentNullException("Application:C2LogixAuthSettings:APIUrl");
                client.BaseAddress = new Uri(apiUrl);

            }).AddHttpMessageHandler<CookieHandler>();

            services.AddMvc()
                 .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                 .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<GetSubmitedProjectsHandler>());

            // Customise default API behavour we dont need MVC ModelState validator
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "C2Logix API", Version = "v1" });
                c.OperationFilter<ExamplesOperationFilter>();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "C2Logix API V1");
                c.DocumentTitle = "Title Documentation";
                c.DocExpansion(DocExpansion.None);

            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
           
        }
    }
}
