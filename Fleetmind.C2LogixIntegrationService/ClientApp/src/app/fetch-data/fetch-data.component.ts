import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { setInterval } from 'timers';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})

export class FetchDataComponent {
  public projects: SubmitedProjects[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<SubmitedProjects[]>(baseUrl + 'api/V1.0/RouteOptimizer/GetSubmitedProjects').subscribe(result => {
      this.projects = result;
    }, error => console.error(error));
    
    // repeat with the interval of 30 seconds
    //let timerId = setInterval(() => {
    //  http.get<SubmitedProjects[]>(baseUrl + 'api/V1.0/RouteOptimizer/GetSubmitedProjects').subscribe(result => {
    //    this.projects = result;
    //    console.log(result);
    //  }, error => console.error(error));
    //}, 30000);
  }

  public isEnabled(id): boolean {
    return !this.projects.filter(item => item.internalRefrenceId == id)[0].isCompleted;
  }
  public okCompleted(id): string{
    if (this.projects.filter(item => item.internalRefrenceId == id)[0].isCompleted)
      return "glyphicon glyphicon-ok";
    else
      return "glyphicon glyphicon-remove";
  }
  public okTransfered(id): string {
    if (this.projects.filter(item => item.internalRefrenceId == id)[0].isTransfered)
      return "glyphicon glyphicon-ok";
    else
      return "glyphicon glyphicon-remove";
  }

  
}

interface SubmitedProjects {
  externalRefrenceId: string;
  internalRefrenceId: string;
  stopNumber: number;
  vehiclesNumber: number;
  facilitiesNumber: number;
  isCompleted: boolean;
  isTransfered: boolean;
  submitDate:string



}
