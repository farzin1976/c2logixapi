import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Route } from '@angular/router';
import { forEach } from '@angular/router/src/utils/collection';


@Component({
  selector: 'app-counter-component',
  templateUrl: './counter.component.html'
})
export class CounterComponent {
  public routes: RouteModel[];
  public stops: StopModel[];
  public vehicles: VehicleModel[];
  public postData: PostDataModel;
  public facilites: FacilityModel[];

  


  private _http: HttpClient;
  private _baseUrl: string;
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<RouteModel[]>(baseUrl + 'api/SampleData/GetRoutes').subscribe(result => {
      this.routes = result;
    }, error => console.error(error));
    http.get<VehicleModel[]>(baseUrl + 'api/SampleData/GetVehicles').subscribe(result => {
      this.vehicles = result;
    }, error => console.error(error));
    http.get<FacilityModel[]>(baseUrl + 'api/SampleData/GetFacilities').subscribe(result => {
      this.facilites = result;
    }, error => console.error(error));
    this._http = http;
    this._baseUrl = baseUrl;
    this.facilites = new Array();
    
  }

  public onChange(event): void {  // event will give you full breif of action
    const newVal = event.target.value;
    this.loadStops(newVal);
    console.log(newVal);
  }



  public loadStops(routeId: string): void {
    this._http.get<StopModel[]>(this._baseUrl + 'api/SampleData/GetStops?id='+routeId).subscribe(result => {
      this.stops = result;
    }, error => console.error(error));
  }

  public SendRequest(): void {
    this.postData = new PostDataModel();
    this.vehicles.forEach((value)=> {
     
      if (value.active) {
       
        this.postData.vehicles.push(value);
      }
    });

    this.postData.facilities = this.facilites;
    this.postData.stops = this.stops;
    this._http.post<PostDataModel>(this._baseUrl + 'api/V1.0/RouteOptimizer/SubmitNewRoute', this.postData).subscribe(result => {
      alert(result);
    }, error => console.error(error));;

    console.log(this.postData);
  }
 
 
}
interface RouteModel {
  id: number;
  name: string;
}

interface StopModel {
  id: number;
  internalRefrenceId: string;
  rmsStopId: number;
  active: boolean;
  city: string;
  comment: string;
  latitude: string;
  longitude: string;
  name: string;
  stateId: string;
  street: string;
  zipCode: string;
  quantity: string;
  originalSequence: number;
  optimizedSequence:number;
}
interface VehicleModel {
  id: number;
  internalRefrenceId: string;
  rmsTruckId: number;
  active: boolean;
  comment: string;
  name: string;
  starts: string;
  ends: string;
  timeToComplete: string;
  startTime: string;
  overTime: string;
  capacity: number;

}
interface FacilityModel {
  id: number;
  active: boolean;
  city: string;
  comment: string;
  latitude: string;
  longitude: string;
  name: string;
  stateId: string;
  street: string;
  zipCode: string;
}

export class PostDataModel {
   stops: StopModel[];
   vehicles: VehicleModel[];
  facilities: FacilityModel[];
  constructor() {
    this.stops = [];
    this.vehicles = [];
    this.facilities = [];
  }

  
  

  

}

