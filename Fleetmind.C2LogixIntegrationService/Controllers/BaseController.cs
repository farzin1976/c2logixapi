﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace Fleetmind.C2LogixIntegrationService.Controllers
{
    [ApiController]
    [Route("api/V1.0/[controller]/[action]")]
    public class BaseController:Controller
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ?? (_mediator = HttpContext.RequestServices.GetService<IMediator>());
    }
}
