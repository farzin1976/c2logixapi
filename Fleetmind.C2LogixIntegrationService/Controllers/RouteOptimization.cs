﻿using Fleetmind.C2LogixIntegrationService.Swagger;
using Fleetmind.Services.integration.Optimization.Application.OptimizedProjects.Commands.Submit;
using Fleetmind.Services.integration.Optimization.Application.OptimizedProjects.Queries;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Examples;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Fleetmind.C2LogixIntegrationService.Controllers
{
    public class RouteOptimization : BaseController
    {
        [HttpGet()]
        [ProducesResponseType(typeof(SubmitedProject), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetSubmitedProject([FromQuery] string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("request is null");
            }
            return Ok(await Mediator.Send(new GetSubmitedProjectQuery() { InternalRefrenceId = id }));
        }

        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<SubmitedProject>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAllSubmitedProjects()
        {
            return Ok(await Mediator.Send(new GetAllSubmitedProjectQuery()));
        }

        [HttpGet()]
        public async Task<IActionResult> GetOptimizationResult([FromQuery] string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("request is null");
            }
            return Ok(await Mediator.Send(new GetOptimizedResultQuery() { InternalRefrenceId = id }));
        }
        [HttpPost("SubmitRoute")]
        [SwaggerRequestExample(typeof(OptimizerRequestModel), typeof(SubmitRouteModelExample))]
        [ProducesResponseType(typeof(SubmisionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Post([FromBody]OptimizerRequestModel optimizeRoute)
        {
            if (optimizeRoute.IsObjectNull())
            {
                return BadRequest("Request is null");
            }
            return Ok(Mediator.Send(new SubmitProjectCommand() { Request = optimizeRoute }).Result);
        }
    }
}
