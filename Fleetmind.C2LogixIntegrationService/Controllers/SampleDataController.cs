using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Fleetmind.C2LogixIntegrationService.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        private static List<Route> Routes = new List<Route>()
        {
            new Route(){Id=1,Name="Route 01"},
            new Route(){Id=2,Name="Route 02"},
            new Route(){Id=3,Name="Route 03"}
        };

        private static List<Stop> stops1 = new List<Stop>()
                 {
                     new Stop(){OriginalSequence=1,Active=true,City="Montreal",Latitude="45.5136560",Longitude="-73.5826830",Name="7966 12TH",StateId="QC",Street="1 12TH AV",Quantity=1},
                     new Stop(){OriginalSequence=2,Active=true,City="Montreal",Latitude="45.5115320",Longitude="-73.5780270",Name="7967 12TH AV",StateId="QC",Street="1 13TH AV",Quantity=1},
                     new Stop(){OriginalSequence=3,Active=true,City="Montreal",Latitude="45.5052730",Longitude="-73.5648730",Name="7968 12TH AV",StateId="QC",Street="1 14TH AV",Quantity=1},
                     new Stop(){OriginalSequence=4,Active=true,City="Montreal",Latitude="45.4993480",Longitude="-73.5542300",Name="7969 12TH AV",StateId="QC",Street="1 15TH AV",Quantity=1},
                      new Stop(){OriginalSequence=5,Active=true,City="Montreal",Latitude="45.5136560",Longitude="-73.5826830",Name="7970 12TH",StateId="QC",Street="1 16TH AV",Quantity=1},
                     new Stop(){OriginalSequence=6,Active=true,City="Montreal",Latitude="45.5115320",Longitude="-73.5780270",Name="7971 12TH AV",StateId="QC",Street="1 17TH AV",Quantity=1},
                     new Stop(){OriginalSequence=7,Active=true,City="Montreal",Latitude="45.5052730",Longitude="-73.5648730",Name="7972 12TH AV",StateId="QC",Street="1 18TH AV",Quantity=1},
                     new Stop(){OriginalSequence=8,Active=true,City="Montreal",Latitude="45.4993480",Longitude="-73.5542300",Name="7973 12TH AV",StateId="QC",Street="1 19TH AV",Quantity=1}
                 };
        private static List<Stop> stops2 = new List<Stop>()
                 {
                     new Stop(){OriginalSequence=1,Active=true,City="Montreal",Latitude="45.5136560",Longitude="-73.5826830",Name="1000",StateId="QC",Street="AV1",Quantity=1},
                     new Stop(){OriginalSequence=2,Active=true,City="Montreal",Latitude="45.5115320",Longitude="-73.5780270",Name="70001",StateId="QC",Street="AV2",Quantity=1},
                     new Stop(){OriginalSequence=3,Active=true,City="Montreal",Latitude="45.5052730",Longitude="-73.5648730",Name="70002",StateId="QC",Street="AV3",Quantity=1},
                     new Stop(){OriginalSequence=4,Active=true,City="Montreal",Latitude="45.4993480",Longitude="-73.5542300",Name="70003",StateId="QC",Street="AV4",Quantity=1},
                      new Stop(){OriginalSequence=5,Active=true,City="Montreal",Latitude="45.5136560",Longitude="-73.5826830",Name="70004",StateId="QC",Street="AV5",Quantity=1},
                     new Stop(){OriginalSequence=6,Active=true,City="Montreal",Latitude="45.5115320",Longitude="-73.5780270",Name="70005",StateId="QC",Street="AV6",Quantity=1},
                     new Stop(){OriginalSequence=7,Active=true,City="Montreal",Latitude="45.5052730",Longitude="-73.5648730",Name="70006",StateId="QC",Street="AV7",Quantity=1},
                     new Stop(){OriginalSequence=8,Active=true,City="Montreal",Latitude="45.4993480",Longitude="-73.5542300",Name="70007",StateId="QC",Street="AV8",Quantity=1}
                 };
        private static List<Stop> stops3 = new List<Stop>()
                 {
                     new Stop(){OriginalSequence=1,Active=true,City="Montreal",Latitude="45.5136560",Longitude="-73.5826830",Name="2000",StateId="QC",Street="AV1",Quantity=1},
                     new Stop(){OriginalSequence=2,Active=true,City="Montreal",Latitude="45.5115320",Longitude="-73.5780270",Name="2001",StateId="QC",Street="AV2",Quantity=1},
                     new Stop(){OriginalSequence=3,Active=true,City="Montreal",Latitude="45.5052730",Longitude="-73.5648730",Name="2002",StateId="QC",Street="AV3",Quantity=1},
                     new Stop(){OriginalSequence=4,Active=true,City="Montreal",Latitude="45.4993480",Longitude="-73.5542300",Name="2003",StateId="QC",Street="AV4",Quantity=1},
                     new Stop(){OriginalSequence=5,Active=true,City="Montreal",Latitude="45.5136560",Longitude="-73.5826830",Name="2004",StateId="QC",Street="AV5",Quantity=1},
                     new Stop(){OriginalSequence=6,Active=true,City="Montreal",Latitude="45.5115320",Longitude="-73.5780270",Name="2005",StateId="QC",Street="AV6",Quantity=1},
                     new Stop(){OriginalSequence=7,Active=true,City="Montreal",Latitude="45.5052730",Longitude="-73.5648730",Name="2006",StateId="QC",Street="AV7",Quantity=1},
                     new Stop(){OriginalSequence=8,Active=true,City="Montreal",Latitude="45.4993480",Longitude="-73.5542300",Name="2007",StateId="QC",Street="AV8",Quantity=1},
                     new Stop(){OriginalSequence=9,Active=true,City="Montreal",Latitude="45.5136560",Longitude="-73.5826830",Name="2008",StateId="QC",Street="AV1",Quantity=1},
                     new Stop(){OriginalSequence=10,Active=true,City="Montreal",Latitude="45.5115320",Longitude="-73.5780270",Name="2009",StateId="QC",Street="AV2",Quantity=1},
                     new Stop(){OriginalSequence=11,Active=true,City="Montreal",Latitude="45.5052730",Longitude="-73.5648730",Name="2010",StateId="QC",Street="AV3",Quantity=1},
                     new Stop(){OriginalSequence=12,Active=true,City="Montreal",Latitude="45.4993480",Longitude="-73.5542300",Name="2011",StateId="QC",Street="AV4",Quantity=1},
                     new Stop(){OriginalSequence=13,Active=true,City="Montreal",Latitude="45.5136560",Longitude="-73.5826830",Name="2012",StateId="QC",Street="AV5",Quantity=1},
                     new Stop(){OriginalSequence=14,Active=true,City="Montreal",Latitude="45.5115320",Longitude="-73.5780270",Name="2013",StateId="QC",Street="AV6",Quantity=1},
                     new Stop(){OriginalSequence=15,Active=true,City="Montreal",Latitude="45.5052730",Longitude="-73.5648730",Name="2014",StateId="QC",Street="AV7",Quantity=1},
                     new Stop(){OriginalSequence=16,Active=true,City="Montreal",Latitude="45.4993480",Longitude="-73.5542300",Name="2015",StateId="QC",Street="AV8",Quantity=1},
                     new Stop(){OriginalSequence=17,Active=true,City="Montreal",Latitude="45.5136560",Longitude="-73.5826830",Name="2016",StateId="QC",Street="AV1",Quantity=1},
                     new Stop(){OriginalSequence=18,Active=true,City="Montreal",Latitude="45.5115320",Longitude="-73.5780270",Name="2017",StateId="QC",Street="AV2",Quantity=1},
                     new Stop(){OriginalSequence=19,Active=true,City="Montreal",Latitude="45.5052730",Longitude="-73.5648730",Name="2018",StateId="QC",Street="AV3",Quantity=1},
                     new Stop(){OriginalSequence=20,Active=true,City="Montreal",Latitude="45.4993480",Longitude="-73.5542300",Name="2019",StateId="QC",Street="AV4",Quantity=1},
                     new Stop(){OriginalSequence=21,Active=true,City="Montreal",Latitude="45.5136560",Longitude="-73.5826830",Name="2020",StateId="QC",Street="AV5",Quantity=1},
                     new Stop(){OriginalSequence=22,Active=true,City="Montreal",Latitude="45.5115320",Longitude="-73.5780270",Name="2021",StateId="QC",Street="AV6",Quantity=1},
                     new Stop(){OriginalSequence=23,Active=true,City="Montreal",Latitude="45.5052730",Longitude="-73.5648730",Name="2022",StateId="QC",Street="AV7",Quantity=1},
                     new Stop(){OriginalSequence=24,Active=true,City="Montreal",Latitude="45.4993480",Longitude="-73.5542300",Name="2023",StateId="QC",Street="AV8",Quantity=1}
                 };
        private static List<Facility> facilities = new List<Facility>()
                 {
                     new Facility(){Active=true,City="Montreal",Latitude="45.5143270",Longitude="-73.5852360",Name="Base #1",StateId="QC",Street="ParkAvenue",ZipCode="H2V 2A1"},
                     new Facility(){Active=true,City="Montreal",Latitude="45.5143270",Longitude="-73.5852360",Name="Base #2",StateId="QC",Street="ParkAvenue",ZipCode="H2V 2A2"},
                     new Facility(){Active=true,City="Montreal",Latitude="45.5143270",Longitude="-73.5852360",Name="Base #3",StateId="QC",Street="ParkAvenue",ZipCode="H2V 2A3"},
                 };
        private static List<Vehicle> vehicles = new List<Vehicle>()
        {
             new Vehicle(){Active=false,Name="30305",TimeToComplete="8:00:00",StartTime="8:00:00",Capacity=10},
             new Vehicle(){Active=false,Name="30306",TimeToComplete="8:00:00",StartTime="8:00:00",Capacity=10},
             new Vehicle(){Active=false,Name="30307",TimeToComplete="8:00:00",StartTime="8:00:00",Capacity=10},
             new Vehicle(){Active=false,Name="30308",TimeToComplete="8:00:00",StartTime="8:00:00",Capacity=10},
        };

        [HttpGet("GetRoutes")]
        public IActionResult GetRoutes()
        {
            return Ok(Routes);
        }
        [HttpGet("GetStops")]
        public IActionResult GetStopes(int id)
        {
            if (id == 1)
                return Ok(stops1);
            if (id == 2)
                return Ok(stops2);
            if (id == 3)
                return Ok(stops3);
            return Ok(stops1);
        }
        [HttpGet("GetVehicles")]
        public ActionResult GetVehicles()
        {
            return Ok(vehicles);
        }

        [HttpGet("GetFacilities")]
        public ActionResult GetFacilitess()
        {
            return Ok(facilities);
        }

        public class Route
        {
            public string Name { get; set; }
            public int Id { get; set; }
        }
        
    }
}
