﻿using System.Collections.Generic;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Swashbuckle.AspNetCore.Examples;

namespace Fleetmind.C2LogixIntegrationService.Swagger
{
    public class SubmitRouteModelExample : IExamplesProvider
    {
        public object GetExamples()
        {
            OptimizerRequestModel _optimizeRoute = new OptimizerRequestModel(
                 new List<Facility>()
                 {
                     new Facility(){Active=true,City="Montreal",Latitude="45.5143270",Longitude="-73.5852360",Name="Base #1",StateId="QC",Street="ParkAvenue",ZipCode="H2V 2A1",IsFacility=true}
                 },
                 new List<Stop>()
                 {
                     new Stop(){Active=true,City="Montreal",Latitude="45.5136560",Longitude="-73.5826830",Name="7966 12TH",StateId="QC",Street="1 12TH AV",Quantity=1},
                     new Stop(){Active=true,City="Montreal",Latitude="45.5115320",Longitude="-73.5780270",Name="7967 12TH AV",StateId="QC",Street="1 13TH AV",Quantity=1},
                     new Stop(){Active=true,City="Montreal",Latitude="45.5052730",Longitude="-73.5648730",Name="7968 12TH AV",StateId="QC",Street="1 14TH AV",Quantity=1},
                     new Stop(){Active=true,City="Montreal",Latitude="45.4993480",Longitude="-73.5542300",Name="7969 12TH AV",StateId="QC",Street="1 15TH AV",Quantity=1},
                 },
                 new List<Vehicle>()
                 {
                     new Vehicle(){Active=true,Name="30305",Starts="Base #1",Ends="Base #1",TimeToComplete="8:00:00",StartTime="8:00:00",Capacity=10,PreferMajorRoads=true}
                 }

                );
            return _optimizeRoute;
        }
    }
}
