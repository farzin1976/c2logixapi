﻿
namespace Fleetmind.Services.Integration.Optimization.FileHelper.Enums
{
    public enum FileType
    {
        CSV,
        Excel,
        Text
    }
}
