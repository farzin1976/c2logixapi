﻿
namespace Fleetmind.Services.Integration.Optimization.FileHelper.Enums
{
    public enum EntityType
    {
        Stop,
        Vehicle,
        Facility
    }
}
