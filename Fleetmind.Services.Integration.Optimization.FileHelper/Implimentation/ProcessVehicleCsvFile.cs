﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using Fleetmind.Services.Integration.Optimization.FileHelper.Configuration;
using Fleetmind.Services.Integration.Optimization.FileHelper.Dto;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Implimentation
{
    public class ProcessVehicleCsvFile : IProcessFile<Vehicle>
    {
        private IFileNameFormat _fileNameFormat;
        private readonly IFileSystem _fileSystem;
        private string _baseLocation;

        public ProcessVehicleCsvFile(IFileNameFormat fileNameFormat, string baseLocation, IFileSystem fileSystem)
        {
            _fileNameFormat = fileNameFormat;
            _baseLocation = baseLocation;
            _fileSystem = fileSystem;
        }

        public string SaveFile(IEnumerable<Vehicle> entities)
        {
            IEnumerable<VehicleDto> vehicleDtos = VehicleDto.CreateList(entities);
            if(!_fileSystem.Directory.Exists(_baseLocation))
            {
                _fileSystem.Directory.CreateDirectory(_baseLocation);
            }
            string filename = _fileNameFormat.GetFileName(Enums.EntityType.Vehicle, Enums.FileType.CSV);
            var file = _fileSystem.FileInfo.FromFileName($@"{_baseLocation}\{filename}");
            
            using (TextWriter textWriter = file.CreateText())
            {
                var csv = new CsvHelper.CsvWriter(textWriter);
                csv.Configuration.AllowComments = true;
                csv.Configuration.RegisterClassMap<CsvVehicleDtoMap>();
                csv.WriteRecords(vehicleDtos);
                textWriter.Close();
            }
            return filename;

        }
    }
}
