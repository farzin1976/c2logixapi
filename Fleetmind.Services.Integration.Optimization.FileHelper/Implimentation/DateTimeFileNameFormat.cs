﻿using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using Fleetmind.Services.Integration.Optimization.FileHelper.Enums;
using System;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Implimentation
{
    public class DateTimeFileNameFormat : IFileNameFormat
    {
        public string GetFileName(EntityType entityType, FileType fileType)
        {
            string fileName = "";
            switch (entityType)
            {
                case EntityType.Stop:
                    fileName = "Stop";
                    break;
                case EntityType.Vehicle:
                    fileName = "Vehicle";
                    break;
                case EntityType.Facility:
                    fileName = "Facility";
                    break;
               
            }
            string fileExtention = "";
            switch (fileType)
            {
                case FileType.CSV:
                    fileExtention = ".CSV";
                    break;
                case FileType.Excel:
                    fileExtention = ".Excel";
                    break;
                case FileType.Text:
                    fileExtention = ".txt";
                    break;
                
            }
            string date = string.Format("{0:yyyy-MM-dd_hh-mm-ss-tt}",DateTime.Now);
            return $"{fileName}_{date}{fileExtention}";


        }
    }
}
