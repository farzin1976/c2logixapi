﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using System.IO.Abstractions;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Implimentation
{
    public class ProcessCsvFileFactory : IProcessFilesFactory
    {
        private IFileNameFormat _fileNameFormat;
        private readonly IFileSystem _fileSystem;
        

        public ProcessCsvFileFactory(IFileNameFormat fileNameFormat, IFileSystem fileSystem)
        {
            _fileNameFormat = fileNameFormat;
            
            _fileSystem = fileSystem;
        }
        public IProcessFile<Facility> ProcessFacilities(string dirPath)
        {
            return new ProcessFacilityCsvFile(_fileNameFormat, dirPath, _fileSystem);
        }

        public IProcessFile<Stop> ProcessStops(string dirPath)
        {
            return new ProcessStopCsvFile(_fileNameFormat, dirPath, _fileSystem);
        }

        public IProcessFile<Vehicle> ProcessVehicles(string dirPath)
        {
            return new ProcessVehicleCsvFile(_fileNameFormat, dirPath, _fileSystem);
        }
    }
}
