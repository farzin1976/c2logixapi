﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using Fleetmind.Services.Integration.Optimization.FileHelper.Configuration;
using Fleetmind.Services.Integration.Optimization.FileHelper.Dto;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Implimentation
{
    public class ProcessStopCsvFile : IProcessFile<Stop>
    {
        private IFileNameFormat _fileNameFormat;
        private readonly IFileSystem _fileSystem;
        private string _baseLocation;

        public ProcessStopCsvFile(IFileNameFormat fileNameFormat, string baseLocation, IFileSystem fileSystem)
        {
            _fileNameFormat = fileNameFormat;
            _baseLocation = baseLocation;
            _fileSystem = fileSystem;
        }

        public string SaveFile(IEnumerable<Stop> entities)
        {
           
            IEnumerable<StopDto> stopDtos = StopDto.CreateList(entities);
            if(!_fileSystem.Directory.Exists(_baseLocation))
            {
                _fileSystem.Directory.CreateDirectory(_baseLocation);
            }
            string filename = _fileNameFormat.GetFileName(Enums.EntityType.Stop, Enums.FileType.CSV);
            var file = _fileSystem.FileInfo.FromFileName($@"{_baseLocation}\{filename}");
            
            using (TextWriter textWriter = file.CreateText())
            {
                var csv = new CsvHelper.CsvWriter(textWriter);
                csv.Configuration.AllowComments = true;
                csv.Configuration.RegisterClassMap<CsvStopsDtoMap>();
                csv.WriteRecords(stopDtos);
                textWriter.Close();
            }
            return filename;
            

        }
    }
}
