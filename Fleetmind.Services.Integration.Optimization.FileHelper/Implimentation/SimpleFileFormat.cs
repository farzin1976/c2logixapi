﻿using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using Fleetmind.Services.Integration.Optimization.FileHelper.Enums;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Implimentation
{
    public class SimpleFileFormat : IFileNameFormat
    {
        public string GetFileName(EntityType entityType, FileType fileType)
        {
            string retValue = "";
            switch (entityType)
            {
                case EntityType.Stop:
                    retValue = "Stop.CSV";
                    break;
                case EntityType.Vehicle:
                    retValue = "Vehicle.CSV";
                    break;
                case EntityType.Facility:
                    retValue = "Facility.CSV";
                    break;
               
               
            }
            return retValue;
        }
    }
}
