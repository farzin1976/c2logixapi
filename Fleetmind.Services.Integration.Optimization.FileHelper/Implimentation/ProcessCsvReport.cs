﻿using CsvHelper;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using Fleetmind.Services.Integration.Optimization.FileHelper.Configuration;
using Fleetmind.Services.Integration.Optimization.FileHelper.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Linq;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Implimentation
{
    public class ProcessCsvReport : IProcessReport<OptimizerResponseModel>
    {
        
        private readonly IFileSystem _fileSystem;
        
        public ProcessCsvReport(IFileSystem fileSystem)
        {
            
            _fileSystem = fileSystem;
            
        }
        public IEnumerable<OptimizerResponseModel> ProcessReport(string fileName, string baseLocation, string internalRefrenceId, string externalRefrenceId)
        {
            var retValue = new List<OptimizerResponseModel>();
           
            var file = _fileSystem.FileInfo.FromFileName($@"{baseLocation}\{fileName}");
           
            if (!file.Exists)
            {
                throw new Exception("File no exist");
            }

            using (var reader = file.OpenText())
            using (var csv = new CsvReader(reader))
            {
                csv.Configuration.AllowComments = true;
                csv.Configuration.RegisterClassMap<CsvOptimizedResultDtoMap>();
                var csvResult = csv.GetRecords<OptimizedResultDto>().ToList();
                retValue = OptimizedResultDto.CreateList(csvResult).ToList();
            }
            
            return retValue.Select(x => { x.InternalRefrenceId = internalRefrenceId;x.ExternalRefrenceId=externalRefrenceId ; return x; });



        }
    }
}
