﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Dto
{
    public class VehicleDto: IEntityDto
    {

        public string Active { get; set; }

        public string Comment { get; set; }

        public string Name { get; set; }
        public string TotalDriveDistance { get; set; }
        public string TotalDriveTime { get; set; }
        public string Facilities { get; set; }

        public string Starts { get; set; }

        public string Ends { get; set; }

        public string TomTomUid { get; set; }

        public string CharacteristicsResource { get; set; }

        public string OpenStart { get; set; }

        public string OpenStop { get; set; }

        public string LoadBeforDepurture { get; set; }

        public string LoadOnReturn { get; set; }

        public string FixedLoadingDuration { get; set; }

        public string LoadingDurationPerUnit { get; set; }

        public string DistanceCost2 { get; set; }

        public string TimeToComplete { get; set; }

        public string StartTime { get; set; }

        public string WorkingDays { get; set; }

        public string OptimalStartTime { get; set; }

        public string BrifingDuration { get; set; }

        public string DeBrifingDuration { get; set; }

        public string LunchTime { get; set; }

        public string LuncDuration { get; set; }

        public string OverTimeHoures { get; set; }

        public string OverTimeCost { get; set; }

        public string Capacity { get; set; }

        public string Capacity2 { get; set; }

        public string Capacity3 { get; set; }

        public string Capacity4 { get; set; }

        public string Capacity5 { get; set; }

        public string Capacity6 { get; set; }

        public string Capacity7 { get; set; }

        public string Capacity8 { get; set; }

        public string SpeedAdjustment { get; set; }

        public string MajorRoads { get; set; }

        public string HourlyCost { get; set; }

        public string CostPerVisit { get; set; }

        public string CostOfUse { get; set; }

        public string NonUsePenalty { get; set; }

        public string PayWholeDay { get; set; }

        public string DistanceCost { get; set; }

        public string DistanceCostThreshold2 { get; set; }

        public string DistanceCost3 { get; set; }

        public string DistanceCostThreshold3 { get; set; }

        public string OverNightDrivingThreshold { get; set; }

        public string MaxNightOutPerJurny { get; set; }

        public string OverNightCost { get; set; }

        public string LegalMaxDriving { get; set; }

        public string LegalMinRestDuration { get; set; }

        public string LegalMaxRestDuration { get; set; }

        public string LegalNightRestDuration { get; set; }

        public string UserData1 { get; set; }

        public string UserData2 { get; set; }

        public string UserData3 { get; set; }

        public string UserData4 { get; set; }

        public string UserData5 { get; set; }

        public string UserData6 { get; set; }

        public static Expression<Func<Vehicle, VehicleDto>> Projection
        {
            get
            {
                return vehicle => new VehicleDto
                {
                    Active = vehicle.Active ? "TRUE" : "FALSE",
                    Name = vehicle.Name,
                    Comment = vehicle.Comment,
                    Capacity = vehicle.Capacity.ToString(),
                    Starts = vehicle.Starts,
                    Ends = vehicle.Ends,
                    StartTime = vehicle.StartTime,
                    TimeToComplete = vehicle.TimeToComplete,
                    MajorRoads = vehicle.PreferMajorRoads ? "TRUE" : "FALSE"

                };
            }
        }
        public static VehicleDto Create(Vehicle vehicle)
        {
            return Projection.Compile().Invoke(vehicle);
        }
        public static IEnumerable<VehicleDto> CreateList(IEnumerable<Vehicle> vehicles)
        {
            HashSet<VehicleDto> vehicleDtos= new HashSet<VehicleDto>();
            foreach (var item in vehicles)
            {
                vehicleDtos.Add(Projection.Compile().Invoke(item));
            }
            return vehicleDtos;
        }
    }
}
