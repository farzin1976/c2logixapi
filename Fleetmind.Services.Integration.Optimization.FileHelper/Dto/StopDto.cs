﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Dto
{
    public class StopDto: IEntityDto
    {
        public string Active { get; set; }

        public string City { get; set; }

        public string Comment { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string Name { get; set; }

        public string StateId { get; set; }

        public string Street { get; set; }

        public string ZipCode { get; set; }

        public string Type { get; set; }

        public string Revenue { get; set; }

        public string Characteristic { get; set; }

        public string AllCharacteristic { get; set; }

        public string Exclude { get; set; }

        public string ServiceSide { get; set; }

        public string FixedVisitDuartion { get; set; }

        public string FixedDurationPerTicket { get; set; }

        public string UnloadingDurationPerUnit { get; set; }

        public string Punctuality { get; set; }

        public string WholeVisitInTimeWindow { get; set; }

        public string PenaltyPerHour { get; set; }

        public string Frequency { get; set; }

        public string PossibleVisitDays1 { get; set; }

        public string TimeWindowStart1 { get; set; }

        public string TimeWindowEnd1 { get; set; }

        public string PossibleVisitDays2 { get; set; }

        public string TimeWindowStart2 { get; set; }

        public string TimeWindowEnd2 { get; set; }

        public string PossibleVisitDays3 { get; set; }

        public string TimeWindowStart3 { get; set; }

        public string TimeWindowEnd3 { get; set; }

        public string MinPartialDuration { get; set; }

        public string MinDuration { get; set; }

        public string Quantity { get; set; }

        public string Quantity2 { get; set; }

        public string Quantity3 { get; set; }

        public string Quantity4 { get; set; }

        public string Quantity5 { get; set; }

        public string Quantity6 { get; set; }

        public string Quantity7 { get; set; }

        public string Quantity8 { get; set; }

        public string Cost { get; set; }

        public string Resources { get; set; }

        public string UserData1 { get; set; }

        public string UserData2 { get; set; }

        public string UserData3 { get; set; }

        public string UserData4 { get; set; }

        public string UserData5 { get; set; }

        public string UserData6 { get; set; }

        public static Expression<Func<Stop, StopDto>> Projection
        {
            get
            {
                return stop => new StopDto
                {
                    Active = stop.Active ? "TRUE" : "FALSE",
                    City = stop.City,
                    Name=stop.Name,
                    StateId = stop.StateId,
                    ZipCode = stop.ZipCode,
                    Street = stop.Street,
                    Comment = stop.Comment,
                    Longitude = stop.Longitude,
                    Latitude = stop.Latitude,
                    Quantity = stop.Quantity.ToString()
                    

                };
            }
        }
        public static StopDto Create(Stop stop)
        {
            return Projection.Compile().Invoke(stop);
        }
        public static IEnumerable<StopDto> CreateList(IEnumerable<Stop> stops)
        {
            HashSet<StopDto> stopDtos = new HashSet<StopDto>();
            foreach (var item in stops)
            {
                stopDtos.Add(Projection.Compile().Invoke(item));
            }
            return stopDtos;
        }
    }
}
