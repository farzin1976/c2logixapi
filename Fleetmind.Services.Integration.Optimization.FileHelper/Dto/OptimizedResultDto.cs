﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Dto
{
    public class OptimizedResultDto: IEntityDto
    {
        public string ProjectName { get; set; }
        public string EventTitle { get; set; }
        public string EventType { get; set; }
        public string ProjectNotes { get; set; }
        public string RouteName { get; set; }
        public string DayId { get; set; }
        public string VehicleName { get; set; }
        public string CustomerName { get; set; }
        public string StopPosition { get; set; }
        public string TsStopType { get; set; }
        public string TypeStops { get; set; }
        public string StopType { get; set; }
        public string StopDriveTime { get; set; }
        public string StopStartTime { get; set; }
        public string StopDriveDistance { get; set; }
        public string StopDuration { get; set; }
        public string StopElapsedDistance { get; set; }
        public string FacilityName { get; set; }
        public string CustomerCity { get; set; }
        public string CustomerStreet { get; set; }
        public string CustomerZipcode { get; set; }
        public string FacilityCity { get; set; }
        public string FacilityStreet { get; set; }
        public string FacilityZipCode { get; set; }
        public string FrequencyStops { get; set; }
        public static Expression<Func<OptimizedResultDto, OptimizerResponseModel>> Projection
        {
            get
            {
                return optimized => new OptimizerResponseModel
                {
                    RouteName=optimized.RouteName,
                    DayId=optimized.DayId,
                    VehicleName=optimized.VehicleName,
                    CustomerName=optimized.CustomerName,
                    StopPosition=optimized.StopPosition,
                    TsStopType=optimized.TsStopType,
                    StopDriveTime=optimized.StopDriveTime,
                    StopStartTime=optimized.StopStartTime,
                    StopDriveDistance=optimized.StopDriveDistance,
                    StopDuration=optimized.StopDuration,
                    StopElapsedDistance=optimized.StopElapsedDistance,
                    FacilityName=optimized.FacilityName

                };
            }
        }

        public static OptimizerResponseModel Create(OptimizedResultDto optimizedResult)
        {
            return Projection.Compile().Invoke(optimizedResult);
        }
        public static IEnumerable<OptimizerResponseModel> CreateList(IEnumerable<OptimizedResultDto> optimizedResults)
        {
            HashSet<OptimizerResponseModel> retValues = new HashSet<OptimizerResponseModel>();
            foreach (var item in optimizedResults)
            {
                retValues.Add(Projection.Compile().Invoke(item));
            }
            return retValues;
        }
    }
}
