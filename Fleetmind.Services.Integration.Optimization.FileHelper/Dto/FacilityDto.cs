﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Dto
{
    public class FacilityDto: IEntityDto
    {
        public string Active { get; set; }

        public string City { get; set; }

        public string Comment { get; set; }

        public string HouseNumber { get; set; }

        public string IsFacility { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string Name { get; set; }

        public string StateId { get; set; }

        public string Street { get; set; }

        public string ZipCode { get; set; }

        public string DaysOpe1 { get; set; }

        public string DaysOpe2 { get; set; }

        public string DaysOpe3 { get; set; }

        public string DaysOpe4 { get; set; }

        public string WindowStart1 { get; set; }

        public string WindowEnd1 { get; set; }

        public string WindowStart2 { get; set; }

        public string WindowEnd2 { get; set; }

        public string WindowStart3 { get; set; }

        public string WindowEnd3 { get; set; }

        public string WindowStart4 { get; set; }

        public string WindowEnd4 { get; set; }

        public string UserData1 { get; set; }

        public string UserData2 { get; set; }

        public string UserData3 { get; set; }

        public string UserData4 { get; set; }

        public string UserData5 { get; set; }

        public string UserData6 { get; set; }

        public static Expression<Func<Facility, FacilityDto>> Projection
        {
            get
            {
                return facility => new FacilityDto
                {
                    Active = facility.Active ? "TRUE" : "FALSE",
                    IsFacility=facility.IsFacility?"TRUE":"FALSE",
                    City = facility.City,
                    Longitude = facility.Longitude,
                    Latitude = facility.Latitude,
                    Name=facility.Name,
                    StateId=facility.StateId,
                    Street=facility.Street,
                    ZipCode=facility.ZipCode

                };
            }
        }
        public static FacilityDto Create(Facility facility)
        {
            return Projection.Compile().Invoke(facility);
        }
        public static IEnumerable<FacilityDto> CreateList(IEnumerable<Facility> facilities)
        {
            HashSet<FacilityDto> facilityDtos = new HashSet<FacilityDto>();
            foreach (var item in facilities)
            {
                facilityDtos.Add(Projection.Compile().Invoke(item));
            }
            return facilityDtos;
        }
    }
}
