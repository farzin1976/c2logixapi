﻿using CsvHelper.Configuration;
using Fleetmind.Services.Integration.Optimization.FileHelper.Dto;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Configuration
{
    class CsvOptimizedResultDtoMap : ClassMap<OptimizedResultDto>
    {
        public CsvOptimizedResultDtoMap()
        {
            Map(m => m.ProjectName).Name("projects_name");
            Map(m => m.EventTitle).Name("event_title");
            Map(m => m.EventType).Name("event_type");
            Map(m => m.RouteName).Name("route_name");
            Map(m => m.DayId).Name("day_id");
            Map(m => m.VehicleName).Name("vehicles_name");
            Map(m => m.CustomerName).Name("customer_name");
            Map(m => m.StopPosition).Name("ts_stop_position");
            Map(m => m.TsStopType).Name("ts_stop_type");
            Map(m => m.TypeStops).Name("type_stops");
            Map(m => m.StopType).Name("stop_type");
            Map(m => m.StopDriveTime).Name("ts_stop_drive_time");
            Map(m => m.StopStartTime).Name("ts_stop_start_time");
            Map(m => m.StopDriveDistance).Name("ts_stop_drive_distance");
            Map(m => m.StopDuration).Name("ts_stop_duration");
            Map(m => m.StopElapsedDistance).Name("ts_stop_elapsed_distance");
            Map(m => m.FacilityName).Name("facilities_name");
            Map(m => m.CustomerCity).Name("customers_city");
            Map(m => m.CustomerStreet).Name("customers_street");
            Map(m => m.CustomerZipcode).Name("customers_zipcode");
            Map(m => m.FacilityCity).Name("facilities_city");
            Map(m => m.FacilityStreet).Name("facilities_street");
            Map(m => m.FacilityZipCode).Name("facilities_zipcode");
            Map(m => m.FrequencyStops).Name("frequency_stops");

        }
    }
}
