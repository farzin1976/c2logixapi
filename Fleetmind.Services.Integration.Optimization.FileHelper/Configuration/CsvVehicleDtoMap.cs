﻿using CsvHelper.Configuration;
using Fleetmind.Services.Integration.Optimization.FileHelper.Dto;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Configuration
{
    public class CsvVehicleDtoMap : ClassMap<VehicleDto>
    {
        public CsvVehicleDtoMap()
        {
            Map(m => m.Active).Name("active");
            Map(m => m.Comment).Name("comment");
            Map(m => m.Name).Name("name");
            Map(m => m.TotalDriveDistance).Name("total_drive_distance");
            Map(m => m.TotalDriveTime).Name("total_drive_time");
            Map(m => m.Facilities).Name("facilities");
            Map(m => m.Starts).Name("starts");
            Map(m => m.Ends).Name("ends");
            Map(m => m.TomTomUid).Name("parameter_tomtom_uid");
            Map(m => m.CharacteristicsResource).Name("parameter_characteristics_resource");
            Map(m => m.OpenStart).Name("parameter_open_start");
            Map(m => m.OpenStop).Name("parameter_open_stop");
            Map(m => m.LoadBeforDepurture).Name("parameter_load_before_departure");
            Map(m => m.LoadOnReturn).Name("parameter_load_on_return_resource");
            Map(m => m.FixedLoadingDuration).Name("parameter_fixed_loading_duration");
            Map(m => m.LoadingDurationPerUnit).Name("parameter_loading_duration_per_unit");
            Map(m => m.DistanceCost2).Name("parameter_distance_cost_2_resource");
            Map(m => m.TimeToComplete).Name("parameter_time_to_complete");
            Map(m => m.StartTime).Name("parameter_start_time");
            Map(m => m.WorkingDays).Name("parameter_working_days_resource");
            Map(m => m.OptimalStartTime).Name("parameter_optimum_start_time_resource");
            Map(m => m.BrifingDuration).Name("parameter_briefing_duration_resource");
            Map(m => m.DeBrifingDuration).Name("parameter_debriefing_duration_resource");
            Map(m => m.LunchTime).Name("parameter_lunch_time_resource");
            Map(m => m.LuncDuration).Name("parameter_lunch_duration_resource");
            Map(m => m.OverTimeHoures).Name("parameter_overtime_hours");
            Map(m => m.OverTimeCost).Name("parameter_overtime_cost");
            Map(m => m.Capacity).Name("parameter_vehicle_capacity");
            Map(m => m.Capacity2).Name("parameter_capacity_2_resource");
            Map(m => m.Capacity3).Name("parameter_capacity_3_resource");
            Map(m => m.Capacity4).Name("parameter_capacity_4_resource");
            Map(m => m.Capacity5).Name("parameter_capacity_5_resource");
            Map(m => m.Capacity6).Name("parameter_capacity_6_resource");
            Map(m => m.Capacity7).Name("parameter_capacity_7_resource");
            Map(m => m.Capacity8).Name("parameter_capacity_2_resource");
            Map(m => m.SpeedAdjustment).Name("parameter_speed_adjustment_resource");
            Map(m => m.MajorRoads).Name("parameter_prefer_major_roads");
            Map(m => m.HourlyCost).Name("parameter_hourly_cost_resource");
            Map(m => m.CostPerVisit).Name("parameter_fixed_cost_per_visit_resource");
            Map(m => m.CostOfUse).Name("parameter_fixed_cost_of_use_resource");
            Map(m => m.NonUsePenalty).Name("parameter_fixed_cost_of_use_resource");
            Map(m => m.PayWholeDay).Name("parameter_pay_whole_day");
            Map(m => m.DistanceCost).Name("parameter_distance_cost_resource");
            Map(m => m.DistanceCostThreshold2).Name("parameter_distance_cost_threshold_2_resource");
            Map(m => m.DistanceCostThreshold3).Name("parameter_distance_cost_threshold_3_resource");
            Map(m => m.OverNightDrivingThreshold).Name("parameter_overnight_driving_threshold_resource");
            Map(m => m.MaxNightOutPerJurny).Name("parameter_max_nights_out_per_journey_resource");
            Map(m => m.OverNightCost).Name("parameter_overnight_cost_resource");
            Map(m => m.LegalMaxDriving).Name("parameter_legal_max_driving_resource");
            Map(m => m.LegalMinRestDuration).Name("parameter_legal_min_rest_duration_resource");
            Map(m => m.UserData1).Name("parameter_user_data1");
            Map(m => m.UserData2).Name("parameter_user_data2");
            Map(m => m.UserData3).Name("parameter_user_data3");
            Map(m => m.UserData4).Name("parameter_user_data4");
            Map(m => m.UserData5).Name("parameter_user_data5");
            Map(m => m.UserData6).Name("parameter_user_data6");



        }
    }
}
