﻿using CsvHelper.Configuration;
using Fleetmind.Services.Integration.Optimization.FileHelper.Dto;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Configuration
{
    public class CsvStopsDtoMap:ClassMap<StopDto>
    {
        public CsvStopsDtoMap()
        {
            Map(m => m.Active).Name("active");
            Map(m => m.City).Name("city");
            Map(m => m.Comment).Name("comment");
            Map(m => m.Latitude).Name("latitude");
            Map(m => m.Longitude).Name("longitude");
            Map(m => m.Name).Name("name");
            Map(m => m.StateId).Name("state_id");
            Map(m => m.Street).Name("street");
            Map(m => m.ZipCode).Name("zipcode");
            Map(m => m.Type).Name("parameter_type_stops");
            Map(m => m.Revenue).Name("parameter_customer_revenue");
            Map(m => m.Characteristic).Name("parameter_characteristics_stops");
            Map(m => m.AllCharacteristic).Name("parameter_all_characteristics_stops");
            Map(m => m.Exclude).Name("parameter_exclude_resources_stops");
            Map(m => m.ServiceSide).Name("parameter_service_side");
            Map(m => m.FixedVisitDuartion).Name("parameter_fixed_visit_duration");
            Map(m => m.FixedDurationPerTicket).Name("parameter_fixed_duration_per_ticket");
            Map(m => m.UnloadingDurationPerUnit).Name("parameter_unloading_duration_per_unit");
            Map(m => m.Punctuality).Name("parameter_punctuality_stops");
            Map(m => m.WholeVisitInTimeWindow).Name("parameter_whole_visit_in_time_window_stops");
            Map(m => m.PenaltyPerHour).Name("parameter_delay_penalty_per_hour_stops");
            Map(m => m.Frequency).Name("parameter_frequency_stops");
            Map(m => m.PossibleVisitDays1).Name("parameter_possible_visit_days_1");
            Map(m => m.TimeWindowStart1).Name("parameter_time_wdw_1_start");
            Map(m => m.TimeWindowEnd1).Name("parameter_time_wdw_1_end");
            Map(m => m.PossibleVisitDays2).Name("parameter_possible_visit_days_2");
            Map(m => m.TimeWindowStart2).Name("parameter_time_wdw_2_start");
            Map(m => m.TimeWindowEnd2).Name("parameter_time_wdw_2_end");
            Map(m => m.PossibleVisitDays3).Name("parameter_possible_visit_days_3");
            Map(m => m.TimeWindowStart3).Name("parameter_time_wdw_3_start");
            Map(m => m.TimeWindowEnd3).Name("parameter_time_wdw_3_end");
            Map(m => m.MinPartialDuration).Name("parameter_min_partial_duration_stops");
            Map(m => m.MinDuration).Name("parameter_min_duration_stops");
            Map(m => m.Quantity).Name("parameter_quantity");
            Map(m => m.Quantity2).Name("parameter_quantity_2");
            Map(m => m.Quantity2).Name("parameter_quantity_2");
            Map(m => m.Quantity3).Name("parameter_quantity_3");
            Map(m => m.Quantity4).Name("parameter_quantity_4");
            Map(m => m.Quantity5).Name("parameter_quantity_5");
            Map(m => m.Quantity6).Name("parameter_quantity_6");
            Map(m => m.Cost).Name("parameter_courier_cost_stops");
            Map(m => m.Resources).Name("parameter_assign_resources");
            Map(m => m.UserData1).Name("parameter_user_data1");
            Map(m => m.UserData2).Name("parameter_user_data2");
            Map(m => m.UserData3).Name("parameter_user_data3");
            Map(m => m.UserData4).Name("parameter_user_data4");
            Map(m => m.UserData5).Name("parameter_user_data5");
            Map(m => m.UserData6).Name("parameter_user_data6");



        }
    }
}
