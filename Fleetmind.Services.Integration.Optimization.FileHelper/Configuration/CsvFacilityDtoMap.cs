﻿using CsvHelper.Configuration;
using Fleetmind.Services.Integration.Optimization.FileHelper.Dto;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Configuration
{
    public class CsvFacilityDtoMap : ClassMap<FacilityDto>
    {
        public CsvFacilityDtoMap()
        {
            Map(m => m.Active).Name("active");
            Map(m => m.City).Name("city");
            Map(m => m.Comment).Name("comment");
            Map(m => m.HouseNumber).Name("house_num");
            Map(m => m.IsFacility).Name("is_facility");
            Map(m => m.Latitude).Name("latitude");
            Map(m => m.Longitude).Name("longitude");
            Map(m => m.Name).Name("name");
            Map(m => m.StateId).Name("state_id");
            Map(m => m.Street).Name("street");
            Map(m => m.ZipCode).Name("zipcode");
            Map(m => m.DaysOpe1).Name("parameter_depot_days_open_1");
            Map(m => m.DaysOpe2).Name("parameter_depot_days_open_2");
            Map(m => m.DaysOpe3).Name("parameter_depot_days_open_3");
            Map(m => m.DaysOpe4).Name("parameter_depot_days_open_4");
            Map(m => m.WindowStart1).Name("parameter_depot_time_wdw_1_start");
            Map(m => m.WindowEnd1).Name("parameter_depot_time_wdw_1_end");
            Map(m => m.WindowStart2).Name("parameter_depot_time_wdw_2_start");
            Map(m => m.WindowEnd2).Name("parameter_depot_time_wdw_2_end");
            Map(m => m.WindowStart3).Name("parameter_depot_time_wdw_3_start");
            Map(m => m.WindowEnd3).Name("parameter_depot_time_wdw_3_end");
            Map(m => m.WindowStart4).Name("parameter_depot_time_wdw_4_start");
            Map(m => m.WindowEnd4).Name("parameter_depot_time_wdw_4_end");
            Map(m => m.UserData1).Name("parameter_user_data1");
            Map(m => m.UserData2).Name("parameter_user_data2");
            Map(m => m.UserData3).Name("parameter_user_data3");
            Map(m => m.UserData4).Name("parameter_user_data4");
            Map(m => m.UserData5).Name("parameter_user_data5");
            Map(m => m.UserData6).Name("parameter_user_data6");



        }
    }
}
