﻿using System.Collections.Generic;


namespace Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions
{
    public interface  IProcessFile<T>
    {
         string SaveFile(IEnumerable<T> entities);
    }
}
