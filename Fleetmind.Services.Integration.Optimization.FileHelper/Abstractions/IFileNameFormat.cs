﻿using Fleetmind.Services.Integration.Optimization.FileHelper.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions
{
    public interface IFileNameFormat
    {
        string GetFileName(EntityType entityType, FileType fileType);
    }
}
