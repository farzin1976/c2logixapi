﻿using Fleetmind.Services.Integration.Optimization.Domain.Entities;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions
{
    public interface IProcessFilesFactory
    {
        IProcessFile<Stop> ProcessStops(string dirPath);
        IProcessFile<Vehicle> ProcessVehicles(string dirPath);
        IProcessFile<Facility> ProcessFacilities(string dirPath);

    }
}
