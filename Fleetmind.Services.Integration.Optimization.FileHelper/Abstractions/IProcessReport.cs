﻿using System.Collections.Generic;

namespace Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions
{
    public interface IProcessReport<T>
    {
        IEnumerable<T> ProcessReport(string fileName, string baseLocation, string internalRefrenceId, string externalRefrenceId);
    }
}
