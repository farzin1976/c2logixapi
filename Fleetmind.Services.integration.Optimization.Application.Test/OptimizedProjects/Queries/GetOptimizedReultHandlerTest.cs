﻿using Fleetmind.Services.integration.Optimization.Application.Abstractions;
using Fleetmind.Services.integration.Optimization.Application.DTO;
using Fleetmind.Services.integration.Optimization.Application.OptimizedProjects.Queries;
using Fleetmind.Services.integration.Optimization.Application.Test.Infrastructure;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.FileHelper.Abstractions;
using Fleetmind.Services.Integration.Optimization.FileHelper.Implimentation;
using Fleetmind.Services.Integration.Optimization.Persistence;
using FleetMind.C2Logix.HttpCalls;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.Protected;
using Shouldly;
using System;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Fleetmind.Services.integration.Optimization.Application.Test.OptimizedProjects.Queries
{
    [Collection("QueryCollection")]
    public class GetOptimizedReultHandlerTest
    {
        private readonly OptimizerDbContext _context;
        private readonly IOptimizerClient _c2Logix;
        private readonly IProcessReport<OptimizerResponseModel> _processReport;
        private readonly ILogger<GetOptimizedReultHandler> _logger;
        private readonly IConfig _config;
        private readonly IFileSystem _fileSystem;
        public GetOptimizedReultHandlerTest(QueryTestFixture fixture)
        {
            _context = fixture.Context;

            #region Setup Http
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               // Setup the PROTECTED method to mock
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               // prepare the expected response of the mocked http call
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.OK


               })
               .Verifiable();

            // use real http client with mocked handler here
            var httpClient = new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri("http://test.com/"),
            };

            var c2LogixApi = new C2LogixApiClient(httpClient);
            #endregion


             _fileSystem = new MockFileSystem();
            var roootDirectory = _fileSystem.Directory.CreateDirectory(@"C:\");

            SimpleFileFormat fileFormat = new SimpleFileFormat();
            _processReport = new ProcessCsvReport(_fileSystem);

            _logger = Mock.Of<ILogger<GetOptimizedReultHandler>>();
            _config = new MockConfig();

        }

        [Fact]
        public async Task Should_Retun_NullResult_When_RefrenceIdIsNotCorrect()
        {
            var su = new GetOptimizedReultHandler(_fileSystem,_context,_c2Logix,_logger,_processReport,_config);
            var result = await su.Handle(new GetOptimizedResultQuery() {InternalRefrenceId="q1we" }, CancellationToken.None);

            result.ShouldBeOfType<OptimizerRespnoseDTO>();
            result.ShouldBeNull();
            
        }

    }
}
