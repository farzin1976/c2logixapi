﻿using Fleetmind.Services.integration.Optimization.Application.OptimizedProjects.Queries;
using Fleetmind.Services.integration.Optimization.Application.Test.Infrastructure;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.Persistence;
using Shouldly;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using System.Linq;

namespace Fleetmind.Services.integration.Optimization.Application.Test.OptimizedProjects.Queries
{
    [Collection("QueryCollection")]
    public class GetAllSubmitedProjectsHandlerTest
    {
        private readonly OptimizerDbContext _context;
        public GetAllSubmitedProjectsHandlerTest(QueryTestFixture fixture)
        {
            _context = fixture.Context;
        }
        [Fact]
        public async Task GetAllProjects()
        {
            var su = new GetAllSubmitedProjectsHandler(_context);
            var result = await su.Handle(new GetAllSubmitedProjectQuery(), CancellationToken.None);
            result.ShouldBeOfType<List<SubmitedProject>>();
            result.Count.ShouldBeGreaterThanOrEqualTo(1);
            result.FirstOrDefault().InternalRefrenceId.ShouldBe("qwe");
        }
    }
}
