﻿using Fleetmind.Services.integration.Optimization.Application.OptimizedProjects.Queries;
using Fleetmind.Services.integration.Optimization.Application.Test.Infrastructure;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;
using Fleetmind.Services.Integration.Optimization.Persistence;
using Shouldly;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Fleetmind.Services.integration.Optimization.Application.Test.OptimizedProjects.Queries
{
    [Collection("QueryCollection")]
    public class GetSubmitedProjectsHandlerTest
    {
        private readonly OptimizerDbContext _context;
        public GetSubmitedProjectsHandlerTest(QueryTestFixture fixture)
        {
            _context = fixture.Context;
        }

        [Fact]
        public async Task GetSubmitedProject()
        {
            var su = new GetSubmitedProjectsHandler(_context);
            var result = await su.Handle(new GetSubmitedProjectQuery() {InternalRefrenceId="qwe" }, CancellationToken.None);
            result.ShouldBeOfType<SubmitedProject>();
            result.ExternalRefrenceId.ShouldBe("123");
        }

    }
}
