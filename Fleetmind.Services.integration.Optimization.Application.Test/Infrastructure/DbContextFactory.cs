﻿using Fleetmind.Services.Integration.Optimization.Persistence;
using System;
using Microsoft.EntityFrameworkCore;
using Fleetmind.Services.Integration.Optimization.Domain.Entities;

namespace Fleetmind.Services.integration.Optimization.Application.Test.Infrastructure
{
    public class DbContextFactory
    {
        public static OptimizerDbContext Create()
        {
            var options = new DbContextOptionsBuilder<OptimizerDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            var context = new OptimizerDbContext(options);
            context.Database.EnsureCreated();
            context.submitedProjects.AddRange(new[]
            {
                new SubmitedProject{ExternalRefrenceId="123",
                                    FacilitiesFileName ="facilites.csv",
                                    InternalRefrenceId ="qwe",
                                    IsCompleted =true,
                                    OptimizationLevel =Integration.Optimization.Domain.Enums.OptimizationLevel.SubmisionStarted
                                    }
            });

            context.SaveChanges();
            return context;

        }

        public static void Destroy(OptimizerDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Dispose();
        }
    }
}
