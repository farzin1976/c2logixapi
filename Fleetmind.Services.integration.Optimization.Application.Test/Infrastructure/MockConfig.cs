﻿using Fleetmind.Services.integration.Optimization.Application.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fleetmind.Services.integration.Optimization.Application.Test.Infrastructure
{
    public class MockConfig : IConfig
    {
        public MockConfig()
        {
            this.Path = @"C:\TestData";
            this.APIUserName = "UserName";
            this.APIPassword = "Password";
        }
        public string Path { get; }

        public string APIUserName { get; }

        public string APIPassword { get; }
    }
}
