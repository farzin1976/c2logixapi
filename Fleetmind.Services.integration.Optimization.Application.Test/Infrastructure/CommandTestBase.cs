﻿using Fleetmind.Services.Integration.Optimization.Persistence;
using System;

namespace Fleetmind.Services.integration.Optimization.Application.Test.Infrastructure
{
    public class CommandTestBase : IDisposable
    {
        protected readonly OptimizerDbContext _context;
        public CommandTestBase()
        {
            _context = DbContextFactory.Create();
        }
        public void Dispose()
        {
            DbContextFactory.Destroy(_context);
        }
    }
}
