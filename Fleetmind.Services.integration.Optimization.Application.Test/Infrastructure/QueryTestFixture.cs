﻿using Fleetmind.Services.Integration.Optimization.Persistence;
using System;
using Xunit;


namespace Fleetmind.Services.integration.Optimization.Application.Test.Infrastructure
{
    public class QueryTestFixture:IDisposable
    {
        public OptimizerDbContext Context { get; private set; }

        public QueryTestFixture()
        {
            Context = DbContextFactory.Create();
        }

        public void Dispose()
        {
            DbContextFactory.Destroy(Context);
        }

    }


    [CollectionDefinition("QueryCollection")]
    public class QueryCollection : ICollectionFixture<QueryTestFixture> { }
}
